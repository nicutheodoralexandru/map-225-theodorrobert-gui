package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Tuple;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.*;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceMessage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NewMessage {
    private MainApp mainApp;
    private Service service;
    private Stage stage;
    private ServiceMessage serviceMessage;


    @FXML
    private Button sendButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextArea textAreaMessage;
    @FXML
    private TextField textFieldTo;

    public NewMessage(MainApp mainApp){
        this.mainApp = mainApp;
        stage = new Stage();
        setService();
        setMessagesService();
    }
    public void ShowStage(){
        stage.show();
    }
    public void setService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        service = new Service(repoUser,repoFriend);
    }
    public void setMessagesService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Repository<Long, Message> repoMessages = new MessageDatabase(url, username, password, new MessageValidator());
        serviceMessage = new ServiceMessage(repoMessages, repoUser, repoFriend);
    }

    @FXML
    private void initialize(){
        onButtonSendClick();
        onButtonCancelClick();

    }
    private void onButtonCancelClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage stage = (Stage) cancelButton.getScene().getWindow();
                stage.close();
            }
        };
        cancelButton.setOnAction(eventHandler);
    }
    private void onButtonSendClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldTo.getText().equals(""))
                    showErrorPopUp("You must select a friend to send a message to!");
                else
                {
                    String[] names = textFieldTo.getText().split(";");
                    Long from = mainApp.getUserID();
                    String message = textAreaMessage.getText();
                    String to="";
                    for(String name: names){
                        String[] parts = name.split(" ");
                        String firstName= parts[0];
                        String lastName = parts[1];
                        Long id = service.findUserByFirstName(firstName,lastName);
                        to += id.toString()+";";
                    }
                    try{
                        serviceMessage.sendMessage(from,to.substring(0,to.length()-1),message);
                        showSignInPopUp("");
                        Stage stage = (Stage) sendButton.getScene().getWindow();
                        stage.close();
                    }catch (ValidationException| RepoException e){
                        showErrorPopUp(e.getMessage());
                    }

                }

            }
        };
        sendButton.setOnAction(eventHandler);
    }
    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();

    }
    public static void showSignInPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sent message");
        alert.setHeaderText("Messge sent successfully!");
        alert.show();
    }
}
