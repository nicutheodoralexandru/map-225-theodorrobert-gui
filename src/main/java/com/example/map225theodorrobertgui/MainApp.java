package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Tuple;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.*;
import com.example.map225theodorrobertgui.observer.Observer;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.EventDatabase;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Notification;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceEvent;
import com.example.map225theodorrobertgui.service.ServiceReports;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MainApp extends Observer {
    private LogIn login;
    private Stage thisStage;
    private Service service;
    private ServiceReports serviceReports;
    private Pagination<Users> usersPagination;
    private int pageSize = 5;
    private int paginationSize = 5;
    ObservableList<Users> users = FXCollections.observableArrayList();

    @FXML
    private Label userLabel;
    @FXML
    private TableColumn<Users,String> tableColumnFirstName;
    @FXML
    private TableColumn<Users,String> tableColumnLastName;
    @FXML
    private TableView<Users> tableViewFriends;
    @FXML
    private Button friendRequestButton;
    @FXML
    private Button friendRequestsButton;
    @FXML
    private Button deleteFriendshipButton;
    @FXML
    private Button eventsButton;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldFirstName;
    @FXML
    private Button logOutButton;
    @FXML
    private Label labelFirstNameUser;
    @FXML
    private Label labelLastNameUser;
    @FXML
    private Button generateReport1Button;
    @FXML
    private Button generateReport2Button;
    @FXML
    private DatePicker startDatePicker;
    @FXML
    private DatePicker endDatePicker;
    @FXML
    private Button startConversationButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button nextButton;
    @FXML
    private Label labelPageNumber;

    @FXML
    private void initialize(){
        initNotifications();
        onClickEventsButton();
        initFriendRequestsButtton();
        onDeleteButtonClick();
        onAddButtonCLick();
        onLogOutButtonClick();
        onStartConversationButtonClick();
        onNextButtonClick();
        onPrevButtonClick();
        onGenerateReportButton1Click();
        onGenerateReportButton2Click();
        String[] parts = login.getEnteredText().split(" ");
        String userName = parts[0];
        String password = parts[1];
        userLabel.setText("Welcome " + userName + "!");

        service.addObserver(this);
        try {

          tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<Users,String>("FirstName"));
          tableColumnLastName.setCellValueFactory(new PropertyValueFactory<Users,String>("SecondName"));
          refreshFriendList();
          textFieldFirstName.textProperty().addListener(o->handleFilter());
          textFieldLastName.textProperty().addListener(o->handleFilter());
          tableViewFriends.setItems(users);



           tableViewFriends.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Users>() {
               @Override
                public void changed(ObservableValue<? extends Users> observable, Users oldValue, Users newValue) {
                   if(newValue != null) {
                       labelFirstNameUser.setText(newValue.getFirstName());
                       labelLastNameUser.setText(newValue.getSecondName());
                   }
                    else{
                       labelFirstNameUser.setText("First Name");
                       labelLastNameUser.setText("Last Name");
                   }
               }
            });
        }catch (RepoException e){
            e.printStackTrace();
        }
    }

    private void initNotifications(){
        /*ServiceEvent tmp = new ServiceEvent(new EventDatabase(Database.url, Database.username, Database.password));
        Notification n = new Notification(tmp, getUserID());
        Notification.stopNotifications = false;
        Thread t = new Thread(n);
        t.start();*/
        ServiceEvent tmp = new ServiceEvent(new EventDatabase(Database.url, Database.username, Database.password, new EventValidator()), service);
        Notification n = new Notification(tmp, getUserID());
        Notification.stopNotifications = false;
    }

    private void onClickEventsButton(){
        MainApp mainApp = this;
        EventHandler<ActionEvent> event1 = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("events.fxml"));
                    fxmlLoader.setController(new Events(mainApp,thisStage));
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("Events");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        eventsButton.setOnAction(event1);
    }

    private void onGenerateReportButton2Click(){
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LocalDate startDate = startDatePicker.getValue();
                LocalDate endDate = endDatePicker.getValue();
                long otherId = service.findUserByFirstName(labelFirstNameUser.getText(),
                labelLastNameUser.getText());
                try{
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Save");
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Pdf", "*.pdf*"));
                    File file = fileChooser.showSaveDialog(thisStage);

                    if (file != null) {
                        serviceReports.generatePdfReport2(
                                file,
                                LogIn.id,
                                otherId,
                                startDate,
                                endDate);
                        update();
                    }
                }catch (RepoException e){
                    showErrorPopUp(e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        generateReport2Button.setOnAction(event);
    }

    private void onGenerateReportButton1Click(){
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LocalDate startDate = startDatePicker.getValue();
                LocalDate endDate = endDatePicker.getValue();
                try{
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Save");
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Pdf", "*.pdf*"));
                    File file = fileChooser.showSaveDialog(thisStage);

                    if (file != null) {
                        serviceReports.generatePdfReport1(
                                file,
                                LogIn.id,
                                startDate,
                                endDate);
                        update();
                    }
                }catch (RepoException e){
                    showErrorPopUp(e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        generateReport1Button.setOnAction(event);
    }

    public MainApp(LogIn login){
        this.login = login;
        thisStage = new Stage();
        setService();
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main-app.fxml"));
            fxmlLoader.setController(this);
            Parent root = (Parent) fxmlLoader.load();

            thisStage.setTitle("MainMenu");
            thisStage.setScene(new Scene(root));
            thisStage.show();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void ShowStage(){
        thisStage.show();
        //update();
    }

    private void onDeleteButtonClick(){


        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String firstName = labelFirstNameUser.getText();
                String lastName = labelLastNameUser.getText();
                try{
                    Long id1 = service.findUserByFirstName(firstName,lastName);
                    service.removeFriendship(getUserID(),id1);
                    labelFirstNameUser.setText("First Name");
                    labelLastNameUser.setText("Last Name");
                    update();

                }catch (RepoException e){
                    showErrorPopUp(e.getMessage());
                }
            }
        };
        deleteFriendshipButton.setOnAction(event);
    }

    private void onAddButtonCLick(){
        MainApp mainApp = this;
        EventHandler<ActionEvent> event1 = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("add-friend.fxml"));
                    fxmlLoader.setController(new AddFriend(mainApp,thisStage));
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("Add Friend");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        friendRequestButton.setOnAction(event1);
    }

    private void onStartConversationButtonClick() {
        MainApp mainApp = this;
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("send-message.fxml"));
                    fxmlLoader.setController(new SendMessage(mainApp, thisStage));
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("Messages");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();
                    //stage.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        startConversationButton.setOnAction(eventHandler);
    }



    private void initFriendRequestsButtton(){
        MainApp mainApp = this;
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("friend-requests.fxml"));
                    fxmlLoader.setController(new FriendRequests(mainApp,thisStage));
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("Friend requests");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        friendRequestsButton.setOnAction(event);
    }

    public void setService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Repository<Long, Message> repoMessage = new MessageDatabase(url, username, password, new MessageValidator());
        service = new Service(repoUser,repoFriend);
        serviceReports = new ServiceReports(repoMessage, repoFriend, repoUser);
    }

    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();

    }

    public  Long getUserID(){
        String[] parts = login.getEnteredText().split(" ");
        String firstName = parts[0];
        String lastName = parts[1];
        return service.findUser(firstName,lastName);
    }

    public void loadTablePage(){

       users.setAll(usersPagination.getItemsOnCurrentPage());
       nextButton.setDisable(!this.usersPagination.hasNextPage());
       prevButton.setDisable(!this.usersPagination.hasPreviousPage());

    }
    private void refreshFriendList(){
        List<Tuple<Users, LocalDate>> friendList = service.getAllFriendsUser(getUserID());
        List<Users> friends = new ArrayList<>();
        for(Tuple<Users,LocalDate> user: friendList){
            friends.add(user.getLeft());
        }

        usersPagination = new Pagination<>(friends,pageSize,paginationSize);

        loadTablePage();

    }

    @Override
    public void update() {

        refreshFriendList();
        tableViewFriends.setItems(users);

    }
    public void onNextButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasNextPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()+1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        nextButton.setOnAction(eventHandler);
    }
    public void onPrevButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasPreviousPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()-1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        prevButton.setOnAction(eventHandler);
    }
    public void onLogOutButtonClick() {
        Stage stage = thisStage;
        EventHandler<ActionEvent> logout = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    Notification.stopNotifications = true;
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("log-in.fxml"));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setTitle("Log in");
                    stage.setScene(new Scene(root));
                    stage.show();
                    Stage stage1 = (Stage) logOutButton.getScene().getWindow();
                    stage1.close();

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        };
        logOutButton.setOnAction(logout);
    }

    public void handleFilter(){
        Predicate<Users> p1 = n->n.getFirstName().startsWith(textFieldFirstName.getText());
        Predicate<Users> p2 = n->n.getSecondName().startsWith(textFieldLastName.getText());
        users.setAll(service.getAllFriendsUser(getUserID()).stream().map(x->x.getLeft()).filter(p1.and(p2)).collect(Collectors.toList()));

    }
}
