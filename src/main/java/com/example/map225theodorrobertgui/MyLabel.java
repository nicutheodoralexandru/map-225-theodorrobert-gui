package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.domain.Message;
import javafx.scene.control.Label;

public class MyLabel extends Label {
    private Message message;

    public MyLabel(String text) {
        super(text);
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
