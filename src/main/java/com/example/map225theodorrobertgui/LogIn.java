package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.FriendshipValidator;
import com.example.map225theodorrobertgui.domain.validators.RepoException;
import com.example.map225theodorrobertgui.domain.validators.UserValidator;
import com.example.map225theodorrobertgui.main.Main;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class LogIn {
    @FXML
    private Hyperlink buttonBack;
    @FXML
    private Button btnLogIN;
    @FXML
    private TextField textFieldUsername;
    @FXML
    private Label usernameError;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private TextField passwordTextFieldVisible;
    @FXML
    private Button passwordVisibilityToggle;
    @FXML
    private Label passwordError;

    public static Long id;
    private boolean passwordVisibility = false;

    @FXML
    public void initialize() {
        passwordTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordTextFieldVisible.setText(newValue);
        });
        passwordTextFieldVisible.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordTextField.setText(newValue);
        });
        passwordVisibilityToggle.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ImageView imageView;
                if(!passwordVisibility) {
                    imageView = new ImageView(new Image(getClass().getResourceAsStream("images/hidden.png")));
                }
                else {
                    imageView = new ImageView(new Image(getClass().getResourceAsStream("images/visible.png")));
                }
                imageView.setFitWidth(20);
                imageView.setFitHeight(20);
                passwordVisibilityToggle.setGraphic(imageView);
                passwordVisibility = !passwordVisibility;
                passwordTextField.setVisible(!passwordVisibility);
                passwordTextFieldVisible.setVisible(passwordVisibility);
            }
        });
    }

    public void OnButtonBackClick(ActionEvent actionEvent) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sign-in.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("SignIn");
            stage.setScene(new Scene(root));
            stage.show();

        }catch (Exception e){
            e.printStackTrace();
        }
        Stage stage = (Stage) buttonBack.getScene().getWindow();
        stage.close();
    }

    public void OnButtonLOGINClick(ActionEvent actionEvent) {
        String url = "jdbc:postgresql://localhost:5432/SocialNetwork";
        String username = "postgres";
        String password = "Legolas@2001";
        Repository<Long, Users> repoUser = new UserDatabase(Database.url, Database.username, Database.password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(Database.url, Database.username, Database.password, new FriendshipValidator());
        Service srv = new Service(repoUser,repoFriend);
        String userName = textFieldUsername.getText();
        String Password = passwordTextField.getText();
        if(userName.length() == 0) {
            // Invalid Username
            usernameError.setVisible(true);
            usernameError.setPadding(new Insets(0, 0, 10, 0));
            textFieldUsername.getStyleClass().add("error");
            textFieldUsername.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    textFieldUsername.getStyleClass().remove("error");
                    usernameError.setVisible(false);
                    usernameError.setPadding(new Insets(0, 0, -5, 0));
                }
            });
        }
        else if(Password.length() == 0) {
            // Invalid Password
            passwordError.setVisible(true);
            passwordError.setPadding(new Insets(0, 0, 10, 0));
            passwordTextField.getStyleClass().add("error");
            passwordTextFieldVisible.getStyleClass().add("error");
            passwordTextField.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    passwordTextField.getStyleClass().remove("error");
                    passwordTextFieldVisible.getStyleClass().remove("error");
                    passwordError.setVisible(false);
                    passwordError.setPadding(new Insets(0, 0, -5, 0));
                }
            });
            passwordTextFieldVisible.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    passwordTextField.getStyleClass().remove("error");
                    passwordTextFieldVisible.getStyleClass().remove("error");
                    passwordError.setVisible(false);
                    passwordError.setPadding(new Insets(0, 0, -5, 0));
                }
            });
        }
        else {
            try {
                id = srv.findUser(userName, Password);
                MainApp mainApp = new MainApp(this);
                mainApp.ShowStage();
                Stage stage = (Stage) buttonBack.getScene().getWindow();
                stage.close();

            } catch (RepoException e) {
                showErrorPopUp(e.getMessage());
            }
        }
    }
    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();

    }
    public String getEnteredText(){
        return textFieldUsername.getText() + " " + passwordTextField.getText();
    }
}
