package com.example.map225theodorrobertgui.test;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.FriendshipValidator;
import com.example.map225theodorrobertgui.domain.validators.MessageValidator;
import com.example.map225theodorrobertgui.domain.validators.UserValidator;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceMessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Test {
    public static void main(String[] args) throws Exception {
        Database.initMysql();
        testMessage();
        System.out.println("All tests done!");
    }

    public static void testMessage() throws Exception {
        UserDatabase userDatabase = new UserDatabase(Database.url, Database.username, Database.password, new UserValidator());
        MessageDatabase messageDatabase = new MessageDatabase(Database.url, Database.username, Database.password, new MessageValidator());
        FriendshipDatabase friendshipDatabase = new FriendshipDatabase(Database.url, Database.username, Database.password, new FriendshipValidator());
        ServiceMessage  serviceMessage = new ServiceMessage(messageDatabase, userDatabase, friendshipDatabase);
        Service service = new Service(userDatabase, friendshipDatabase);
        //add data for tests
        Users a = new Users("test11", "test11", "t1");
        service.addUser(a.getFirstName(), a.getSecondName(), a.getUserName(), "1");
        Users b = new Users("test22", "test22", "t2");
        service.addUser(b.getFirstName(), b.getSecondName(), a.getUserName(), "2");
        Users c = new Users("test33", "test33", "t3");
        service.addUser(c.getFirstName(), c.getSecondName(), a.getUserName(), "3");
        service.addFriendship(service.findUser(a.getFirstName(), a.getSecondName()),
                service.findUser(b.getFirstName(), b.getSecondName()));
        service.addFriendship(service.findUser(c.getFirstName(), c.getSecondName()),
                service.findUser(b.getFirstName(), b.getSecondName())); 
        service.addFriendship(service.findUser(a.getFirstName(), a.getSecondName()),
                service.findUser(c.getFirstName(), c.getSecondName()));
        //

        //cleanup data from tests
        service.removeFriendship(service.findUser(a.getFirstName(), a.getSecondName()),
                service.findUser(b.getFirstName(), b.getSecondName()));
        service.removeFriendship(service.findUser(c.getFirstName(), c.getSecondName()),
                service.findUser(b.getFirstName(), b.getSecondName()));
        service.removeFriendship(service.findUser(a.getFirstName(), a.getSecondName()),
                service.findUser(c.getFirstName(), c.getSecondName()));
        service.removeUser(service.findUser(a.getFirstName(), a.getSecondName()));
        service.removeUser(service.findUser(b.getFirstName(), b.getSecondName()));
        service.removeUser(service.findUser(c.getFirstName(), c.getSecondName()));
    }

    public static void testServiceFriendshipRequest(){
    }

    public static void testService(){
        //load properties
        Properties properties = new Properties();
        ClassLoader test = Thread.currentThread().getContextClassLoader();

        InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("resources/mysql.properties");
        InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        try {
            properties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //start db
        String url = properties.getProperty("url");
        String username = properties.getProperty("username");
        String password = properties.getProperty("password");
        Repository<Long, Users> repoUser = new UserDatabase(url, username, password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Service srv = new Service(repoUser,repoFriend);
        //test getFriendshipsOfUserId
        //add test users
        Users u1 = new Users("A1", "B1","A1B1");
        Users u2 = new Users("A2", "B2","A2B2");
        Users u3 = new Users("A3", "B3","a3b3");
        Users u4 = new Users("A4", "B4","a4b4");
        Users u5 = new Users("A5", "B5","a5b5");
        repoUser.save(u1);
        repoUser.save(u2);
        repoUser.save(u3);
        repoUser.save(u4);
        repoUser.save(u5);
        //add test friendships
        Friendship f1 = new Friendship(u1.getId(), u2.getId(), LocalDate.now());
        Friendship f2 = new Friendship(u1.getId(), u3.getId(), LocalDate.now());
        Friendship f3 = new Friendship(u1.getId(), u4.getId(), LocalDate.now());
        Friendship f4 = new Friendship(u2.getId(), u3.getId(), LocalDate.now());
        Friendship f5 = new Friendship(u4.getId(), u5.getId(), LocalDate.now());
        repoFriend.save(f1);
        repoFriend.save(f2);
        repoFriend.save(f3);
        repoFriend.save(f4);
        repoFriend.save(f5);
        //
        try{
            List<Friendship> f = srv.getFriendshipsOfUserId(u1.getId());
            if(f.size() != 3)
                throw new Exception();
            if(!(f.contains(f1) && f.contains(f2) && f.contains(f3)))
                throw new Exception();
        }catch(Exception e){
            e.printStackTrace();
        }
        //done
        System.out.println("Service tests done...");
    }
}
