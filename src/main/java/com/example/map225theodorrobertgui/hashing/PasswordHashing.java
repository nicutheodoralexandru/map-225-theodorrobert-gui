package com.example.map225theodorrobertgui.hashing;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PasswordHashing {

    public PasswordHashing() {
    }

    public static byte[] get_SHA_256(String password) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(password.getBytes(StandardCharsets.UTF_8));
    }

    public static String toHexString(byte[] hash) throws NoSuchAlgorithmException{
        BigInteger number = new BigInteger(1,hash);
        StringBuilder hexString = new StringBuilder(number.toString(16));
        while (hexString.length() < 64)
        {
            hexString.insert(0,'0');
        }
        return hexString.toString();
    }
    public String hashPassword(String password)throws NoSuchAlgorithmException{
        return toHexString(get_SHA_256(password));
    }
}
