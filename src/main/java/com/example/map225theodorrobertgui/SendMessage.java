package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Tuple;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.*;
import com.example.map225theodorrobertgui.observer.Observer;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceMessage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SendMessage extends Observer {
    private  MainApp mainApp;
    private Service service;
    private Stage thisStage;
    private ServiceMessage serviceMessage;
    private Pagination<Users> usersPagination;
    private int pageSize = 5;
    private int paginationSize = 5;

    String firstName;
    String lastName;
    Message message;
    ObservableList<Users> friends = FXCollections.observableArrayList();
    ObservableList<MyLabel> messages = FXCollections.observableArrayList();

    @FXML
    private TableView tableViewFriends;
    @FXML
    private TableColumn columnFirstName;
    @FXML
    private TableColumn columnLastName;
    @FXML
    private TableColumn columnUserName;
    @FXML
    private TextArea textAreaMessage;
    @FXML
    private Button sendMessageButton;
    @FXML
    private SplitPane splitPane;
    @FXML
    private Button buttonBack;
    @FXML
    private Button nextButton;
    @FXML
    private Button prevButton;
    @FXML
    private Label labelPageNumber;
    @FXML
    private Button replyAllButton;
    @FXML
    private Button replyButton;
    @FXML
    private Button newMessageButton;
    @FXML
    private ListView chatBox;
    @FXML
    private Button conversationBack;

    public SendMessage(MainApp mainApp,Stage stage){
        this.mainApp = mainApp;
        this.thisStage = stage;

        setService();
        setMessagesService();
    }
    public void setService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        service = new Service(repoUser,repoFriend);
    }
    public void setMessagesService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Repository<Long, Message> repoMessages = new MessageDatabase(url, username, password, new MessageValidator());
        serviceMessage = new ServiceMessage(repoMessages, repoUser, repoFriend);
    }
    @FXML
    private void initialize(){
        onButtonSendClick();
       // onToTextFieldTextChanged();
        onButtonBackClick();
        Long id = mainApp.getUserID();
        onNextButtonClick();
        onPrevButtonClick();
        onReplyButtonClick();
        onReplyAllButtonClick();
        onConversationBackButtonClick();
        onNewMessageButtonClick();
        conversationBack.setVisible(false);
        replyAllButton.setDisable(true);
        chatBox.setVisible(false);
        replyButton.setDisable(true);

        try{

            columnFirstName.setCellValueFactory(new PropertyValueFactory<Users,String>("FirstName"));
            columnLastName.setCellValueFactory(new PropertyValueFactory<Users,String>("SecondName"));
            columnUserName.setCellValueFactory(new PropertyValueFactory<Users,String>("UserName"));
            refreshFriendList();
            tableViewFriends.setItems(friends);
            tableViewFriends.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Users>() {

                @Override
                public void changed(ObservableValue<? extends Users> observable, Users oldValue, Users newValue) {
                    if(newValue !=null)
                    {
                         firstName = newValue.getFirstName();
                         lastName = newValue.getSecondName();
                        Long id1 = service.findUserByFirstName(firstName,lastName);
                        tableViewFriends.setVisible(false);
                        prevButton.setVisible(false);
                        nextButton.setVisible(false);
                        labelPageNumber.setVisible(false);
                        chatBox.setVisible(true);
                        conversationBack.setVisible(true);
                        initChatBox(id1);

                    }
                }
            });
            chatBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MyLabel>() {

                @Override
                public void changed(ObservableValue<? extends MyLabel> observable, MyLabel oldValue, MyLabel newValue) {
                    if(newValue != null){
                        message = newValue.getMessage();
                        if(newValue.getMessage().getTo().equals(mainApp.getUserID().toString())){
                            replyButton.setDisable(false);

                        }
                        else
                         if(message.getFrom() != mainApp.getUserID())
                        {
                            replyAllButton.setDisable(false);
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void onNewMessageButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("new-message.fxml"));
                    fxmlLoader.setController(new NewMessage(mainApp));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.setTitle("New message");
                    stage.show();



                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        newMessageButton.setOnAction(eventHandler);
    }
    private void onConversationBackButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                tableViewFriends.setVisible(true);
                prevButton.setVisible(true);
                nextButton.setVisible(true);
                labelPageNumber.setVisible(true);
                chatBox.setVisible(false);
                conversationBack.setVisible(false);
            }
        };
        conversationBack.setOnAction(eventHandler);
    }
    private void onReplyAllButtonClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String m = textAreaMessage.getText();
                Long reply = message.getId();
                Long from = mainApp.getUserID();
                String to = message.getFrom().toString();
                String[] parts = message.getTo().split(";");
                for(String part : parts){
                    if(!part.equals(mainApp.getUserID().toString())){
                        to += ";" + part;
                    }
                }
                try{
                    serviceMessage.replyAll(from,to,m,reply);
                    textAreaMessage.setText("");
                    replyAllButton.setDisable(true);
                    initChatBox(message.getFrom());
                }catch (ValidationException e){
                    showErrorPopUp(e.getMessage());
                }
            }
        };
        replyAllButton.setOnAction(eventHandler);
    }

    public void loadTablePage(){

        friends.setAll(usersPagination.getItemsOnCurrentPage());
        nextButton.setDisable(!this.usersPagination.hasNextPage());
        prevButton.setDisable(!this.usersPagination.hasPreviousPage());
    }
    private void refreshFriendList(){
        List<Tuple<Users, LocalDate>> friendList = service.getAllFriendsUser(mainApp.getUserID());
        List<Users> friendsUser = new ArrayList<>();
        for(Tuple<Users,LocalDate> user: friendList){
            friendsUser.add(user.getLeft());
        }
        usersPagination = new Pagination<>(friendsUser,pageSize,paginationSize);
        loadTablePage();
    }
    public void onNextButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasNextPage()){
                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()+1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        nextButton.setOnAction(eventHandler);
    }
    public void onPrevButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasPreviousPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()-1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        prevButton.setOnAction(eventHandler);
    }
    private void initChatBox(Long id){

        List<Message> conversation = serviceMessage.getConversation(mainApp.getUserID().toString(),id.toString());
        List<MyLabel> labels = new ArrayList<>();
        conversation.forEach(x ->{
            MyLabel label = new MyLabel(service.findOne(x.getFrom()).getUserName() +": "+x.getMessage()+ "\n" + "Sent at: " + x.getDate().getHour() + ":" + x.getDate().getMinute() +"  ");
            label.setMessage(x);
            if(x.getFrom() == mainApp.getUserID()){
                label.setStyle("-fx-border-width: 1;-fx-border-color: #5304c2; -fx-font-family: Lexend-Bold; -fx-text-fill: black");
                label.setAlignment(Pos.BASELINE_RIGHT);
            }
            else
            {
                label.setStyle("-fx-border-width: 1;-fx-border-color: red;-fx-font-family: Lexend-Bold; -fx-text-fill: black");
                label.setAlignment(Pos.BASELINE_LEFT);
            }
            labels.add(label);

        });
//        ScrollPane container = new ScrollPane();
//        VBox chatBox = new VBox(5);
//        container.setPrefHeight(300.0);
//        container.setPrefWidth(300.0);
//        chatBox.setPrefHeight(300.0);
//        chatBox.setPrefWidth(300.0);
//        chatBox.setSpacing(10);
//        container.setContent(chatBox);
//        chatBox.setPadding(new Insets(5,0,0,5));


        messages.clear();
        messages.setAll(labels);
        //chatBox.getItems().clear();
        chatBox.setItems(messages);


//        for(Message m : conversation){
//
//            if(m.getFrom() == mainApp.getUserID())
//            {
//                Label label1 = new Label("You: ");
//                Label label = new Label(m.getMessage());
//
//                HBox hBox = new HBox();
//                HBox hBox1 = new HBox();
//                hBox1.setPrefWidth(300.0);
//                hBox.setPrefWidth(300.0);
//                hBox1.setStyle("-fx-border-width: 2; -fx-border-color: black");
//                hBox.getChildren().add(label1);
//                hBox1.getChildren().add(label);
//                hBox.setAlignment(Pos.BASELINE_RIGHT);
//                hBox1.setAlignment(Pos.BASELINE_RIGHT);
//                chatBox.getChildren().add(hBox);
//                chatBox.getChildren().add(hBox1);
//                chatBox.setSpacing(10);
//
//            }
//            else
//            if(m.getFrom() == id){
//                Label label = new Label(m.getMessage());
//                Label label1 = new Label(service.findOne(m.getFrom()).getUserName() + " :");
//
//                HBox hBox = new HBox();
//                HBox hBox1 = new HBox();
//                hBox1.setPrefWidth(300.0);
//                hBox.setPrefWidth(300.0);
//                hBox.getChildren().add(label1);
//                hBox1.getChildren().add(label);
//                hBox1.setStyle("-fx-border-width: 2; -fx-border-color: black");
//                hBox.setAlignment(Pos.BASELINE_LEFT);
//                chatBox.getChildren().add(hBox);
//                chatBox.getChildren().add(hBox1);
//                chatBox.setSpacing(10);
//            }
//
//        }
        //splitPane.getItems().set(0, chatBox);
    }
    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();

    }
    private void onButtonSendClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String names = firstName + " " + lastName;
                if (!names.equals("")) {
                    String[] parts = names.split(";");
                    String to = "";
                    for (String part : parts) {
                        String[] name = part.split(" ");
                        String FirstName = name[0];
                        String LastName = name[1];
                        Long id;
                        try {
                            id = service.findUserByFirstName(FirstName, LastName);
                            to += id.toString() + ";";
                        }catch (RepoException e){
                            showErrorPopUp(e.getMessage());
                        }

                    }
                    String message = textAreaMessage.getText();
                    Long from = mainApp.getUserID();

                    try {
                        String toR = to.substring(0,to.length()-1);
                        serviceMessage.sendMessage(from, toR, message);



                        initChatBox(Long.parseLong(to.substring(0,to.length()-1)));
                        textAreaMessage.setText("");

                    } catch (RepoException | ValidationException e) {
                        showErrorPopUp(e.getMessage());
                    }
                }
                else
                {
                    showErrorPopUp("You must introduce at least a user!");
                }
            }
        };


        sendMessageButton.setOnAction(eventHandler);
    }
    private void onReplyButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                String m = textAreaMessage.getText();
                Long reply = message.getId();
                Long to = message.getFrom();
                Long from = mainApp.getUserID();
                try{
                    serviceMessage.replyMessage(from,to,m,reply);
                    textAreaMessage.setText("");
                    replyButton.setDisable(true);
                    initChatBox(message.getFrom());

                }catch (ValidationException e){
                    showErrorPopUp(e.getMessage());

                }
            }
        };
        replyButton.setOnAction(eventHandler);
    }

    private void onButtonBackClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    try{
                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main-app.fxml"));
                        fxmlLoader.setController(mainApp);
                        Parent root = (Parent) fxmlLoader.load();
                        thisStage.setTitle("MainMenu");
                        thisStage.setScene(new Scene(root));
                        thisStage.show();
                        //stage.close();
                    } catch (Exception e){
                        e.printStackTrace();
                    }

            }
        };
        buttonBack.setOnAction(eventHandler);
    }

    @Override
    public void update() {


    }
}
