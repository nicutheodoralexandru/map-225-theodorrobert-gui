package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.FriendshipValidator;
import com.example.map225theodorrobertgui.domain.validators.RepoException;
import com.example.map225theodorrobertgui.domain.validators.UserValidator;
import com.example.map225theodorrobertgui.domain.validators.ValidationException;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class SignIn {

    @FXML
    private TextField textFieldFirstName;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldUsername;
    @FXML
    private Button btnSignIN;
    @FXML
    private Hyperlink buttonBack;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private TextField passwordTextFieldVisible;
    @FXML
    private Button passwordVisibilityToggle;
    @FXML
    private PasswordField confirmPasswordTextField;
    @FXML
    private TextField confirmPasswordTextFieldVisible;
    @FXML
    private Button confirmPasswordVisibilityToggle;
    @FXML
    private Label firstNameError;
    @FXML
    private Label lastNameError;
    @FXML
    private Label usernameError;
    @FXML
    private Label passwordError;
    @FXML
    private Label confirmPasswordError;

    private boolean passwordVisibility = false;
    private boolean confirmPasswordVisibility = false;

    @FXML
    public void initialize() {
        passwordTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordTextFieldVisible.setText(newValue);
        });
        passwordTextFieldVisible.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordTextField.setText(newValue);
        });
        passwordVisibilityToggle.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ImageView imageView;
                if(!passwordVisibility) {
                    imageView = new ImageView(new Image(getClass().getResourceAsStream("images/hidden.png")));
                }
                else {
                    imageView = new ImageView(new Image(getClass().getResourceAsStream("images/visible.png")));
                }
                imageView.setFitWidth(20);
                imageView.setFitHeight(20);
                passwordVisibilityToggle.setGraphic(imageView);
                passwordVisibility = !passwordVisibility;
                passwordTextField.setVisible(!passwordVisibility);
                passwordTextFieldVisible.setVisible(passwordVisibility);
            }
        });
        confirmPasswordTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            confirmPasswordTextFieldVisible.setText(newValue);
        });
        confirmPasswordTextFieldVisible.textProperty().addListener((observable, oldValue, newValue) -> {
            confirmPasswordTextField.setText(newValue);
        });
        confirmPasswordVisibilityToggle.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ImageView imageView;
                if(!confirmPasswordVisibility) {
                    imageView = new ImageView(new Image(getClass().getResourceAsStream("images/hidden.png")));
                }
                else {
                    imageView = new ImageView(new Image(getClass().getResourceAsStream("images/visible.png")));
                }
                imageView.setFitWidth(20);
                imageView.setFitHeight(20);
                confirmPasswordVisibilityToggle.setGraphic(imageView);
                confirmPasswordVisibility = !confirmPasswordVisibility;
                confirmPasswordTextField.setVisible(!confirmPasswordVisibility);
                confirmPasswordTextFieldVisible.setVisible(confirmPasswordVisibility);
            }
        });
    }

    public void OnButtonSignINClick(ActionEvent actionEvent) {
//        Properties properties = new Properties();
//        String url;
//        String username;
//        String password;
//        ClassLoader test = Thread.currentThread().getContextClassLoader();
//
//        InputStream stream = Thread.currentThread().getContextClassLoader()
//                .getResourceAsStream("resources/com.example.map225theodorrobertgui/postgresql.properties");
//
//        try {
//            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
//            properties.load(reader);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //start db
        Repository<Long, Users> repoUser = new UserDatabase(Database.url, Database.username, Database.password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(Database.url, Database.username, Database.password, new FriendshipValidator());
        Service srv = new Service(repoUser,repoFriend);
        try{
            String FirstName;
            String LastName;
            FirstName = textFieldFirstName.getText();
            LastName = textFieldLastName.getText();
            String userName = textFieldUsername.getText();
            String Password = passwordTextField.getText();
            String confirmPassword = confirmPasswordTextField.getText();
            if(Password.length() == 0) {
                // Invalid Password
                passwordError.setVisible(true);
                passwordError.setPadding(new Insets(0, 0, 10, 0));
                passwordTextField.getStyleClass().add("error");
                passwordTextFieldVisible.getStyleClass().add("error");
                passwordTextField.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        passwordTextField.getStyleClass().remove("error");
                        passwordTextFieldVisible.getStyleClass().remove("error");
                        passwordError.setVisible(false);
                        passwordError.setPadding(new Insets(0, 0, -5, 0));
                    }
                });
                passwordTextFieldVisible.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        passwordTextField.getStyleClass().remove("error");
                        passwordTextFieldVisible.getStyleClass().remove("error");
                        passwordError.setVisible(false);
                        passwordError.setPadding(new Insets(0, 0, -5, 0));
                    }
                });
            }
            else if(!Password.equals(confirmPassword)){
                // Invalid Password
                confirmPasswordError.setVisible(true);
                confirmPasswordError.setPadding(new Insets(0, 0, 10, 0));
                confirmPasswordTextField.getStyleClass().add("error");
                confirmPasswordTextFieldVisible.getStyleClass().add("error");
                confirmPasswordTextField.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        confirmPasswordTextField.getStyleClass().remove("error");
                        confirmPasswordTextFieldVisible.getStyleClass().remove("error");
                        confirmPasswordError.setVisible(false);
                        confirmPasswordError.setPadding(new Insets(0, 0, -5, 0));
                    }
                });
                confirmPasswordTextFieldVisible.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        confirmPasswordTextField.getStyleClass().remove("error");
                        confirmPasswordTextFieldVisible.getStyleClass().remove("error");
                        confirmPasswordError.setVisible(false);
                        confirmPasswordError.setPadding(new Insets(0, 0, -5, 0));
                    }
                });
                confirmPasswordTextField.setText("");
            }
            else {
                srv.addUser(FirstName, LastName, userName, Password);
                showSignInPopUp(FirstName + " " + LastName);
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("hello-view.fxml"));
                    Parent root = (Parent) fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.setTitle("MainMenu");
                    stage.setScene(new Scene(root));
                    stage.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Stage stage = (Stage) btnSignIN.getScene().getWindow();
                stage.close();
            }

        }catch (ValidationException e){
            showInvalidDataErrors(e.getMessage());
        }catch (RepoException | IllegalArgumentException e) {
            showErrorPopUp(e.getMessage());
        }


    }

    public void showInvalidDataErrors(String message) {
        Integer error = Integer.parseInt(message);
        if(error % 10 == 1) {
            // Invalid First Name
            firstNameError.setVisible(true);
            firstNameError.setPadding(new Insets(0, 0, 10, 0));
            textFieldFirstName.getStyleClass().add("error");
            textFieldFirstName.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    textFieldFirstName.getStyleClass().remove("error");
                    firstNameError.setVisible(false);
                    firstNameError.setPadding(new Insets(0, 0, -5, 0));
                }
            });
        }
        if(error / 10 % 10 == 1) {
            // Invalid Last Name
            lastNameError.setVisible(true);
            lastNameError.setPadding(new Insets(0, 0, 10, 0));
            textFieldLastName.getStyleClass().add("error");
            textFieldLastName.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    textFieldLastName.getStyleClass().remove("error");
                    lastNameError.setVisible(false);
                    lastNameError.setPadding(new Insets(0, 0, -5, 0));
                }
            });
        }
        if(error / 100 % 10 == 1) {
            // Invalid Username
            usernameError.setVisible(true);
            usernameError.setPadding(new Insets(0, 0, 10, 0));
            textFieldUsername.getStyleClass().add("error");
            textFieldUsername.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    textFieldUsername.getStyleClass().remove("error");
                    usernameError.setVisible(false);
                    usernameError.setPadding(new Insets(0, 0, -5, 0));
                }
            });
        }
    }

    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();
    }

    public static void showSignInPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sign up complete");
        alert.setHeaderText("Account created successfully!");
        alert.setContentText("Your account was created successfully!\nWelcome to our community, " + message + " !");
        alert.show();
    }

    public void OnButtonBackClick(ActionEvent actionEvent) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("log-in.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Log in");
            stage.setScene(new Scene(root));
            stage.show();

        }catch (Exception e){
            e.printStackTrace();
        }
        Stage stage = (Stage) buttonBack.getScene().getWindow();
        stage.close();
    }
    public void showPasswordNotMatching(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText("The two password don't match!\n");
        alert.show();
    }
}
