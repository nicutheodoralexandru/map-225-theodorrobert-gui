package com.example.map225theodorrobertgui.repository.memory;

import com.example.map225theodorrobertgui.domain.Entity;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepo <ID,E extends Entity<ID>> implements Repository<ID,E> {

    private Map<ID,E> entities;
    private Validator<E> validator;

    public InMemoryRepo(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    /**
     * Function that finds an object based on its id
     * @param id: the id of the object to be returned
     * @return
     * throws IllegalArgumentException if id is null
     */

    @Override
    public E findOne(ID id) {
        if(id == null)
            throw  new IllegalArgumentException("Id must not be null!\n");
        return entities.get(id);
    }
    /**
     * Function that finds an object in the repository
     * @param entity: the object to be returned
     * @return
     * throws IllegalArgumentException if entity is null
     */
    @Override
    public E findOne(E entity){
        if(entity == null)
            throw  new IllegalArgumentException("Entity must not be null!\n");
        for(Map.Entry<ID,E> entry: entities.entrySet()){
            if(entry.getValue().equals(entity))
                return entry.getValue();

        }
       /* if(entities.containsValue(entity))
            return entity;*/
        return null;
    }

    /**
     * Function that returns all object from the repository
     * @return returns all objects of type user or friendship from the repository
     */
    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    /**
     * Function that saves an object in the repository
     * @param entity: the object to be saved in the repository
     * @return
     * throws IllegalArgumentException if entity is null
     */
    @Override
    public E save(E entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        if(findOne(entity)!=null)
            return entity;
        entities.put(entity.getId(),entity);
        return null;
    }

    /**
     * Function that removes object from repository based on its id
     * @param id: the id of the object to be removed from the repository
     * @return
     * throws IllegalArgumentException if id is null
     */
    @Override
    public E remove(ID id) {
        if(id == null)
            throw  new IllegalArgumentException("Id must not be null!\n");
        E entity = this.findOne(id);
        if(entity != null)
        {
            entities.remove(id,entity);
            return entity;
        }
        return null;
    }
    /**
     * Function that removes object from repository based on its id
     * @param entity: the object to be removed from the repository
     * @return
     * throws IllegalArgumentException if entity is null
     */
    @Override
    public E remove(E entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        if(this.findOne(entity) != null)
        {
            entities.remove(entity);
            return entity;
        }
        return null;
    }

    @Override
    public E update(E entity) {
        if(entity == null)
            throw  new IllegalArgumentException("Id must not be null!\n");
        validator.validate(entity);
        if(this.findOne(entity)!=null){
            entities.put(entity.getId(),entity);
            return entity;
        }
        return null;


    }

    /**
     * Function that returns the number of objects in the repository
     * @return returns the number of objects of type user or friendship from the repository
     */
    @Override
    public int size() {
        return entities.size();
    }

    @Override
    public void setUserPassword(E entity, String passwd) {

    }

    @Override
    public String getPassword(E entity) {
        return null;
    }

}
