package com.example.map225theodorrobertgui.repository.file;

import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.Validator;

import java.util.List;

public class UserFile extends AbstractFileRepo<Long, Users> {

    public UserFile(Validator<Users> validator, String fileName) {
        super(validator, fileName);
    }

    @Override
    protected String createEntityAsString(Users entity) {

        return entity.getId()+";"+entity.getFirstName()+";"+entity.getSecondName();
    }

    @Override
    protected Users extractEntity(List<String> attributes) {
        Users user = new Users(attributes.get(1),attributes.get(2),attributes.get(3));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }
}
