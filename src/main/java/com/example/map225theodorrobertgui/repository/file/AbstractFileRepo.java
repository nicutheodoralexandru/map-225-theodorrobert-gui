package com.example.map225theodorrobertgui.repository.file;

import com.example.map225theodorrobertgui.domain.Entity;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.memory.InMemoryRepo;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * Class for file repository
 * @param <ID>: the id of the object
 * @param <E>: the object itself which can be a user or a friendship
 */
public abstract class AbstractFileRepo<ID,E extends Entity<ID>> extends InMemoryRepo<ID,E> {

    private String fileName;

    /**
     * Constructor of AbstractFileRepo that initializes a file repository
     * @param validator: the validator for the objects added in the repository
     * @param fileName: the name of the file where the objects are saved
     */
    public AbstractFileRepo(Validator<E> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        this.loadData();
    }

    /**
     * Function that creates and returns a string from the fields of an object
     * @param entity: the object being from the string is being created
     * @return returns a string from the fields of the repository
     */
    protected abstract String createEntityAsString(E entity);

    /**
     * Function that extracts the fields of an object from a string,creates the object and returns it
     * @param attributes: the string from which we extract the objects fields
     * @return returns an object from a string after extracting its fields from the string
     */
    protected abstract E extractEntity(List<String> attributes);

    /**
     * Function that writes an object in a file
     * @param entity: the object to be written in the file
     */
    protected void writeToFile(E entity){
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,true))){
            bufferedWriter.write(createEntityAsString(entity));
            bufferedWriter.newLine();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Function that loads the objects from the file in the repository
     */
    private void loadData(){
        try(BufferedReader buff = new BufferedReader(new FileReader(this.fileName))){
            String line;
            while((line=buff.readLine())!=null){
                List<String> args = Arrays.asList(line.split(";"));
                E entity = extractEntity(args);
                super.save(entity);
            }
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Function that refreshes the file when a remove function is called
     */
    private void refreshFile(){
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,false))){
            for(E ent: this.findAll()) {
                bufferedWriter.write(createEntityAsString(ent));
                bufferedWriter.newLine();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    @Override
    public E save(E entity){
        E ent = super.save(entity);
        if(ent == null){
            writeToFile(entity);
        }
        return ent;
    }
    @Override
    public E remove(ID id){
        E ent = super.remove(id);
        if(ent != null){
            refreshFile();
        }
        return ent;
    }
    @Override
    public E remove(E entity){
        E ent = super.remove(entity);
        if(ent != null){
            refreshFile();
        }
        return ent;
    }
    @Override
    public E update(E entity){
        E ent = super.update(entity);
        if(ent!=null){
            refreshFile();
        }
        return ent;
    }

}
