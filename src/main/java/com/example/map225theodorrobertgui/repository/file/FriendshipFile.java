package com.example.map225theodorrobertgui.repository.file;

import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.validators.Validator;

import java.time.LocalDate;
import java.util.List;

public class FriendshipFile extends AbstractFileRepo<Long, Friendship> {

    public FriendshipFile(Validator<Friendship> validator, String fileName) {
        super(validator, fileName);
    }

    @Override
    protected String createEntityAsString(Friendship entity) {
        return entity.getPair().getFirst() + ";" + entity.getPair().getSecond()+";"+entity.getFriendshipDate();
    }

    @Override
    protected Friendship extractEntity(List<String> attributes) {
        Friendship friendship = new Friendship(Long.parseLong(attributes.get(0)),Long.parseLong(attributes.get(1)), LocalDate.parse(attributes.get(2)));
        return friendship;
    }
}
