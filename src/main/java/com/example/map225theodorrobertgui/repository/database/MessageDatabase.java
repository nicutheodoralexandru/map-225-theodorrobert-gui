package com.example.map225theodorrobertgui.repository.database;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class MessageDatabase implements Repository<Long, Message> {
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;

    public MessageDatabase(String url, String username, String password, Validator<Message> validator) {
        this.url = Database.url;
        this.username = Database.username;
        this.password = Database.password;
        this.validator = validator;
    }
    @Override
    public Message findOne(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("Id must not be null!\n");

        Message messageUser;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Message\" where \"id\" = ?");
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id = resultSet.getLong("id");
                Long from = resultSet.getLong("from");
                String to = resultSet.getString("to");
                String message = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long reply = resultSet.getLong("reply");
                messageUser = new Message(from,to,message,date,reply);
                messageUser.setId(id);
                return messageUser;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Message findOne(Message entity) {
        if(entity == null)
            throw new IllegalArgumentException("Id must not be null!\n");

        Message messageUser;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Message\" where \"from\"=? AND \"to\"=? AND \"message\"=?");
            statement.setLong(1,entity.getFrom());
            statement.setString(2,entity.getTo());
            statement.setString(3, entity.getMessage());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id = resultSet.getLong("id");
                Long from = resultSet.getLong("from");
                String to = resultSet.getString("to");
                String message = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long reply = resultSet.getLong("reply");
                messageUser = new Message(from,to,message,date,reply);
                messageUser.setId(id);
                return messageUser;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Message\"");
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                Long from = resultSet.getLong("from");
                String to = resultSet.getString("to");
                String message = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long reply = resultSet.getLong("reply");
                Message messageUser = new Message(from,to,message,date,reply);
                messageUser.setId(id);
                messages.add(messageUser);
            }
            return messages;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return messages;
    }

    @Override
    public Message save(Message entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "insert into \"Message\" (\"from\", \"to\", \"message\", \"date\", \"reply\") values (?, ?, ?, ?, ?)";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getFrom());
            ps.setString(2,entity.getTo());
            ps.setString(3, entity.getMessage());
            Timestamp timestamp = Timestamp.valueOf(entity.getDate());
            ps.setTimestamp(4,timestamp);
            ps.setLong(5,entity.getReply());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Message remove(Long aLong) {
        return null;
    }

    @Override
    public Message remove(Message entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "delete from \"Message\" where \"from\"=? AND \"to\"=? AND \"message\"=?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getFrom());
            ps.setString(2,entity.getTo());
            ps.setString(3,entity.getMessage());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Message update(Message entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "UPDATE \"Message\" SET \"message\"=? WHERE \"from\"=? AND \"to\"=?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1,entity.getMessage());
            ps.setLong(2,entity.getFrom());
            ps.setString(3,entity.getTo());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int size() {
       Set<Message> messages = (Set<Message>) findAll();
        return messages.size();
    }

    @Override
    public void setUserPassword(Message entity, String passwd) {
    }

    @Override
    public String getPassword(Message entity) {
        return null;
    }
}
