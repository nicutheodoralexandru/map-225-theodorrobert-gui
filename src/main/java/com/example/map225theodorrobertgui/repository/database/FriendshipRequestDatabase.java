package com.example.map225theodorrobertgui.repository.database;

import com.example.map225theodorrobertgui.domain.FriendshipRequest;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class FriendshipRequestDatabase implements Repository<Long, FriendshipRequest> {
    private String url;
    private String username;
    private String password;
    private Validator<FriendshipRequest> validator;

    public FriendshipRequestDatabase(String url, String username, String password, Validator<FriendshipRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public FriendshipRequest findOne(Long id) {
        if(id < 1)
            return null;
        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Request\" where \"id\"=?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String status = resultSet.getString("status");
                LocalDate date = LocalDate.parse(resultSet.getString("date"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                return new FriendshipRequest(id, id1, id2, status, date);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendshipRequest findOne(FriendshipRequest entity) {
        return findOne(entity.getId());
    }

    @Override
    public Iterable<FriendshipRequest> findAll() {
        Set<FriendshipRequest> friendshipRequests = new HashSet<>();
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Request\"");
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String status = resultSet.getString("status");
                LocalDate date = LocalDate.parse(resultSet.getString("date"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                FriendshipRequest friendshipRequest = new FriendshipRequest(id, id1, id2, status, date);
                friendshipRequests.add(friendshipRequest);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return friendshipRequests;
    }

    @Override
    public FriendshipRequest save(FriendshipRequest entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "insert into \"Request\" (\"id\", \"id1\", \"id2\", \"status\", \"date\") values (?, ?, ?, ?, ?)";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getId());
            ps.setLong(2,entity.getPair().getFirst());
            ps.setLong(3,entity.getPair().getSecond());
            ps.setString(4,entity.getStatus());
            ps.setString(5,entity.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendshipRequest remove(Long id) {
        String sql = "delete from \"Request\" where \"id\"=?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1, id);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendshipRequest remove(FriendshipRequest entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        return remove(entity.getId());
    }

    @Override
    public FriendshipRequest update(FriendshipRequest entity) {
        String sql = "update \"Request\" set \"id\"=?, \"id1\"=?, \"id2\"=?, \"status\"=?, \"date\"=? where \"id\"=?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1, entity.getId());
            ps.setLong(2, entity.getPair().getFirst());
            ps.setLong(3, entity.getPair().getSecond());
            ps.setString(4, entity.getStatus());
            ps.setString(5, entity.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            ps.setLong(6, entity.getId());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int size() {
        Set<FriendshipRequest> friendshipRequests = (Set<FriendshipRequest>) this.findAll();
        return friendshipRequests.size();
    }

    @Override
    public void setUserPassword(FriendshipRequest entity, String passwd) {

    }

    @Override
    public String getPassword(FriendshipRequest entity) {
        return null;
    }
}
