package com.example.map225theodorrobertgui.repository.database;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class FriendshipDatabase implements Repository<Long, Friendship> {
    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;

    public FriendshipDatabase(String url, String username, String password, Validator<Friendship> validator) {
        this.url = Database.url;
        this.username = Database.username;
        this.password = Database.password;
        this.validator = validator;
    }
    @Override
    public Friendship findOne(Long aLong) {
        return null;
    }

    @Override
    public Friendship findOne(Friendship entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        Friendship friendship;
        validator.validate(entity);
        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Friendship\" where \"id1\"=? AND \"id2\"=? OR \"id1\"=? AND \"id2\"=?");
            statement.setLong(1,entity.getPair().getFirst());
            statement.setLong(2,entity.getPair().getSecond());
            statement.setLong(3,entity.getPair().getSecond());
            statement.setLong(4,entity.getPair().getFirst());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id1 = resultSet.getLong("ID1");
                Long id2 = resultSet.getLong("ID2");
                String date = resultSet.getString("date");
                friendship = new Friendship(id1,id2, LocalDate.parse(date,DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                return friendship;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Friendship\"");
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id1 = resultSet.getLong("ID1");
                Long id2 = resultSet.getLong("ID2");
                String date = resultSet.getString("date");
                Friendship friendship = new Friendship(id1,id2,LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                friendships.add(friendship);
            }
            return friendships;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "insert into \"Friendship\" (\"id1\", \"id2\", \"date\") values (?, ?, ?)";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getPair().getFirst());
            ps.setLong(2,entity.getPair().getSecond());
            ps.setString(3,entity.getFriendshipDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship remove(Long aLong) {
        return null;
    }

    @Override
    public Friendship remove(Friendship entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "delete from \"Friendship\" where \"id1\" = ? AND \"id2\" = ? OR \"id1\" = ? AND \"id2\" = ?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getPair().getFirst());
            ps.setLong(2,entity.getPair().getSecond());
            ps.setLong(4,entity.getPair().getFirst());
            ps.setLong(3,entity.getPair().getSecond());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship update(Friendship entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "update \"Friendship\" set \"id1\"=?, \"id2\"=?, \"date\"=?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getPair().getFirst());
            ps.setLong(2,entity.getPair().getSecond());
            ps.setString(3,entity.getFriendshipDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int size() {
        Set<Friendship> friendships = (Set<Friendship>) this.findAll();
        return friendships.size();
    }

    @Override
    public void setUserPassword(Friendship entity, String passwd) {

    }

    @Override
    public String getPassword(Friendship entity) {
        return null;
    }
}
