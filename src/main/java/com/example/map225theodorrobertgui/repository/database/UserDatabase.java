package com.example.map225theodorrobertgui.repository.database;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class UserDatabase implements Repository<Long, Users> {

    private String url;
    private String username;
    private String password;
    private Validator<Users> validator;

    public UserDatabase(String url, String username, String password, Validator<Users> validator) {
        this.url = Database.url;
        this.username = Database.username;
        this.password = Database.password;
        this.validator = validator;
    }

    @Override
    public Users findOne(Long aLong) {

        if(aLong == null)
            throw new IllegalArgumentException("Id must not be null!\n");

        Users user;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Users\" where \"ID\" = ?");
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String userName = resultSet.getString("UserName");
                user = new Users(firstName,lastName,userName);
                user.setId(id);
                return user;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Users findOne(Users entity) {

        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        Users user;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Users\" where \"UserName\" = ?");
            statement.setString(1,entity.getUserName());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String userName = resultSet.getString("UserName");
                user = new Users(firstName,lastName,userName);
                user.setId(id);
                return user;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Users> findAll() {
        Set<Users> users = new HashSet<>();
        try(Connection connection = DriverManager.getConnection(Database.url,Database.username,Database.password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Users\"");
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id = resultSet.getLong("ID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String userName = resultSet.getString("UserName");
                Users user = new Users(firstName,lastName,userName);
                user.setId(id);
                users.add(user);
            }
            return users;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Users save(Users entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "insert into \"Users\" (\"FirstName\", \"LastName\", \"UserName\") values (?, ?, ?)";
        try(Connection connection = DriverManager.getConnection(Database.url,Database.username,Database.password);
        PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1,entity.getFirstName());
            ps.setString(2,entity.getSecondName());
            ps.setString(3, entity.getUserName());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Users remove(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("Id must not be null!\n");
        String sql = "delete from \"Users\" where \"ID\" = ?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
        PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,aLong);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Users remove(Users entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "delete from \"Users\" where \"UserName\" = ?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1,entity.getUserName());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Users update(Users entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "UPDATE \"Users\" SET  \"FirstName\" = ?, \"LastName\" = ? WHERE \"UserName\" = ?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1,entity.getFirstName());
            ps.setString(2,entity.getSecondName());
            ps.setString(3,entity.getUserName());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int size() {
        Set<Users> users = (Set<Users>) this.findAll();
        return users.size();
    }
    public void setUserPassword(Users entity, String passwd){
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        String sql = "UPDATE \"Users\" SET  \"Password\" = ? WHERE \"UserName\" = ?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1,passwd);
            ps.setString(2, entity.getUserName());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public String getPassword(Users entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        validator.validate(entity);
        Users user;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Users\" where \"UserName\" = ?");
            statement.setString(1,entity.getUserName());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){

                String password = resultSet.getString("Password");
                return password;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
