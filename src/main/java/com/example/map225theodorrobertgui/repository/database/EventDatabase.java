package com.example.map225theodorrobertgui.repository.database;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Event;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.Validator;
import com.example.map225theodorrobertgui.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventDatabase implements Repository<Long, Event> {
    private String url;
    private String password;
    private String username;
    private Validator<Event> eventValidator;

    public EventDatabase(String url, String username, String password, Validator<Event> validator){
        this.url = url;
        this.password = password;
        this.username = username;
        this.eventValidator = validator;
    }

    @Override
    public Event findOne(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("Id must not be null!\n");

        Event event;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Event\" where \"id\" = ?");
            statement.setLong(1,aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                Long id = resultSet.getLong("id");
                String atendees = resultSet.getString("atendees");
                String location = resultSet.getString("location");
                Long organizerId = resultSet.getLong("organizerId");
                LocalDate date = LocalDate.parse(resultSet.getString("date"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                event = new Event(id, atendees, organizerId, date, location);
                return event;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Event findOne(Event entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        Event event;

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = connection.prepareStatement
                    ("SELECT * from \"Event\" where \"id\" = ? and \"atendees\"=? and \"organizerId\"=? and \"date\"=? and \"location\"=?");
            statement.setLong(1,entity.getId());
            statement.setString(2,entity.getAtendees());
            statement.setLong(3,entity.getOrganizerId());
            statement.setString(4,entity.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            statement.setString(5,entity.getLocation());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                return entity;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Event> findAll() {
        Set<Event> events = new HashSet<>();
        try(Connection connection = DriverManager.getConnection(Database.url,Database.username,Database.password);
            PreparedStatement statement = connection.prepareStatement("SELECT * from \"Event\"");
            ResultSet resultSet = statement.executeQuery()){
            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                String atendees = resultSet.getString("atendees");
                Long organizerId = resultSet.getLong("organizerId");
                LocalDate date = LocalDate.parse(resultSet.getString("date"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                String location = resultSet.getString("location");
                Event event = new Event(id, atendees, organizerId, date, location);
                events.add(event);
            }
            return events;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return events;
    }

    @Override
    public Event save(Event entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        eventValidator.validate(entity);
        String sql = "insert into \"Event\" (\"id\", \"atendees\", \"organizerId\", \"date\", \"location\") values (?, ?, ?, ?, ?)";
        try(Connection connection = DriverManager.getConnection(Database.url,Database.username,Database.password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1,entity.getId());
            ps.setString(2,entity.getAtendees());
            ps.setLong(3, entity.getOrganizerId());
            ps.setString(4, entity.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            ps.setString(5, entity.getLocation());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Event remove(Long aLong) {
        if(aLong == null)
            throw new IllegalArgumentException("Id must not be null!\n");
        String sql = "delete from \"Event\" where \"id\" = ?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setLong(1, aLong);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Event remove(Event entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        remove(entity.getId());
        return null;
    }

    @Override
    public Event update(Event entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must not be null!\n");
        eventValidator.validate(entity);
        String sql = "update \"Event\" set  \"atendees\"=?, \"organizerId\"=?, \"date\"=?, \"location\"=? where \"id\"=?";
        try(Connection connection = DriverManager.getConnection(url,username,password);
            PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1,entity.getAtendees());
            ps.setLong(2, entity.getOrganizerId());
            ps.setString(3, entity.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
            ps.setString(4, entity.getLocation());
            ps.setLong(5, entity.getId());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int size() {
        Set<Event> events = (Set<Event>) this.findAll();
        return events.size();
    }

    @Override
    public void setUserPassword(Event entity, String passwd) {

    }

    @Override
    public String getPassword(Event entity) {
        return null;
    }
}
