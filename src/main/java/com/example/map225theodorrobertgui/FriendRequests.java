package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.FriendshipRequest;
import com.example.map225theodorrobertgui.domain.FriendshipRequestUI;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.FriendshipRequestValidator;
import com.example.map225theodorrobertgui.domain.validators.FriendshipValidator;
import com.example.map225theodorrobertgui.domain.validators.UserValidator;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.FriendshipRequestDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceFriendshipRequest;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class FriendRequests {
    private Stage thisStage;
    private MainApp parentStage;
    private ServiceFriendshipRequest serviceFriendshipRequest;
    private Pagination<FriendshipRequestUI> requestPagination;
    private int pageSize = 2;
    private int paginationSize = 2;
    private Service service;
    private ObservableList<FriendshipRequestUI> friendshipRequests = FXCollections.observableArrayList();

    @FXML
    private TableView<FriendshipRequestUI> friendRequestsTable;
    @FXML
    private TableColumn<FriendshipRequestUI, String> id1Column;
    @FXML
    private TableColumn<FriendshipRequestUI, String> id2Column;
    @FXML
    private TableColumn<FriendshipRequestUI, String> statusColumn;
    @FXML
    private TableColumn<FriendshipRequestUI, LocalDate> dateColumn;
    @FXML
    private Button goBackButton;
    @FXML
    private Button rejectFriendRequestButton;
    @FXML
    private Button acceptFriendRequestButton;
    @FXML
    private Button cancelFriendRequestButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button nextButton;
    @FXML
    private Label labelPageNumber;

    public FriendRequests(MainApp parentStage,Stage stage){
        service = new Service(new UserDatabase(Database.url, Database.username, Database.password, new UserValidator()),
                new FriendshipDatabase(Database.url, Database.username, Database.password, new FriendshipValidator()));
        serviceFriendshipRequest = new ServiceFriendshipRequest(new FriendshipRequestDatabase(Database.url,
                Database.username, Database.password, new FriendshipRequestValidator()), service);
        this.parentStage = parentStage;
       thisStage = stage;
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("friend-requests.fxml"));
            fxmlLoader.setController(this);
            Parent root = (Parent) fxmlLoader.load();

            thisStage.setTitle("Cereri prietenie");
            thisStage.setScene(new Scene(root));
            thisStage.show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    private void initialize(){
        friendRequestsTable();
        refreshTable();
        friendRequestsTable.setItems(friendshipRequests);
        goBackButton();
        onNextButtonClick();
        onPrevButtonClick();
        rejectFriendRequestButton();
        acceptFriendRequestButton();
        cancelFriendRequestButton();

    }
    public void loadTablePage(){

        friendshipRequests.setAll(requestPagination.getItemsOnCurrentPage());
        nextButton.setDisable(!this.requestPagination.hasNextPage());
        prevButton.setDisable(!this.requestPagination.hasPreviousPage());
    }
    public void onNextButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(requestPagination.hasNextPage()){

                    requestPagination.setCurrentPage(requestPagination.getCurrentPage()+1);
                    labelPageNumber.setText((requestPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        nextButton.setOnAction(eventHandler);
    }
    public void onPrevButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(requestPagination.hasPreviousPage()){

                    requestPagination.setCurrentPage(requestPagination.getCurrentPage()-1);
                    labelPageNumber.setText((requestPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        prevButton.setOnAction(eventHandler);
    }

    private void acceptFriendRequestButton(){
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Long id1 = friendRequestsTable.getSelectionModel().getSelectedItem().getPair().getFirst();
                if(id1 == LogIn.id)
                    return;
                Long id2 = friendRequestsTable.getSelectionModel().getSelectedItem().getPair().getSecond();
                FriendshipRequest f = serviceFriendshipRequest.getFriendRequestsFrom(id1).stream()
                        .filter(x -> x.getPair().getSecond() == id2).collect(Collectors.toList()).get(0);
                serviceFriendshipRequest.approveRequest(f.getId());
                try {
                    service.addFriendship(f.getPair().getFirst(), f.getPair().getSecond());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                refreshTable();
            }
        };
        acceptFriendRequestButton.setOnAction(event);
    }

    private void friendRequestsTable(){
        //id1Column.setCellValueFactory(x -> new SimpleLon);
        id1Column.setCellValueFactory(p -> new SimpleStringProperty( p.getValue().getPairNames().getFirst()));
        id2Column.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getPairNames().getSecond()));
        statusColumn.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getStatus()));
        dateColumn.setCellValueFactory(p -> new SimpleObjectProperty<>(p.getValue().getDate()));

    }

    private void refreshTable(){
        List<FriendshipRequestUI> l = serviceFriendshipRequest.getFriendRequestsUITo(LogIn.id);
        l.addAll(serviceFriendshipRequest.getFriendRequestsUIFrom(LogIn.id));
        requestPagination = new Pagination<>(l,pageSize,paginationSize);
        loadTablePage();

    }

    private void rejectFriendRequestButton(){
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(friendRequestsTable.getSelectionModel().getSelectedItems().size() == 1){
                    Long id1 = friendRequestsTable.getSelectionModel().getSelectedItem().getPair().getFirst();
                    if(id1 == LogIn.id)
                        return;
                    Long id2 = friendRequestsTable.getSelectionModel().getSelectedItem().getPair().getSecond();
                    FriendshipRequest f = serviceFriendshipRequest.getFriendRequestsFrom(id1).stream()
                            .filter(x -> x.getPair().getSecond() == id2).collect(Collectors.toList()).get(0);
                    serviceFriendshipRequest.rejectRequest(f.getId());
                    refreshTable();
                }
            }
        };
        rejectFriendRequestButton.setOnAction(event);
    }

    private void cancelFriendRequestButton(){
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(friendRequestsTable.getSelectionModel().getSelectedItems().size() == 1){
                    Long id1 = LogIn.id;
                    Long id2 = friendRequestsTable.getSelectionModel().getSelectedItem().getPair().getSecond();
                    serviceFriendshipRequest.cancelFriendRequest(id1, id2);
                    refreshTable();
                }
            }
        };
        cancelFriendRequestButton.setOnAction(event);
    }

    private void goBackButton(){
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main-app.fxml"));
                    fxmlLoader.setController(parentStage);
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("MainMenu");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        goBackButton.setOnAction(event);
    }

    public void showStage(){
        thisStage.show();
    }
}
