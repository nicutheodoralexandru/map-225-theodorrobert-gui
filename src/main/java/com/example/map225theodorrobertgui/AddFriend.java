package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.FriendshipRequest;
import com.example.map225theodorrobertgui.domain.Tuple;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.FriendshipRequestValidator;
import com.example.map225theodorrobertgui.domain.validators.FriendshipValidator;
import com.example.map225theodorrobertgui.domain.validators.RepoException;
import com.example.map225theodorrobertgui.domain.validators.UserValidator;
import com.example.map225theodorrobertgui.observer.Observer;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.FriendshipRequestDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceFriendshipRequest;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AddFriend extends Observer {
    private MainApp mainApp;
    private Stage thisStage;
    private Service service;
    private ServiceFriendshipRequest srvRequest;
    private Pagination<Users> usersPagination;
    private int pageSize = 5;
    private int paginationSize = 5;
    ObservableList<Users> users = FXCollections.observableArrayList();
    @FXML
    private Button prevButton;
    @FXML
    private Button nextButton;
    @FXML
    private Label labelPageNumber;

    @FXML
    private Label userLabel;
    @FXML
    private TableColumn<Users,String> tableColumnFirstName;
    @FXML
    private TableColumn<Users,String> tableColumnLastName;
    @FXML
    private TableView<Users> tableViewUsers;
    @FXML
    private Button friendRequestButton;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldFirstName;
    @FXML
    private Button backButton;
    @FXML
    private Label labelFirstNameUser;
    @FXML
    private Label labelLastNameUser;
    @FXML
    private VBox mainAppLeftScrollPaneFirstNameVBox;
    @FXML
    private VBox mainAppLeftScrollPaneLastNameVBox;
    @FXML
    private VBox mainAppLeftScrollPaneCheckBoxVBox;


    public AddFriend(MainApp mainApp,Stage stage){
        this.mainApp = mainApp;
        thisStage = stage;

    }
    public void ShowStage(){
        thisStage.show();
    }
    @FXML
    private void initialize(){
        setService();
        setSrvRequest();
        onNextButtonClick();
        onPrevButtonClick();
        service.addObserver(this);
        srvRequest.addObserver(this);
        userLabel.setText("Current user: " + service.findUser(mainApp.getUserID()).getFirstName() + " " + service.findUser(mainApp.getUserID()).getSecondName());
        List<Users> usersList = service.getAllUsersNotFriendsWithUser(mainApp.getUserID());
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<Users,String>("FirstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<Users,String>("SecondName"));
        refreshFriendList();
        tableViewUsers.setItems(users);
        textFieldFirstName.textProperty().addListener(o->handleFilter());
        textFieldLastName.textProperty().addListener(o->handleFilter());
        tableViewUsers.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Users>() {
            @Override
            public void changed(ObservableValue<? extends Users> observable, Users oldValue, Users newValue) {
                if(newValue != null){
                    labelFirstNameUser.setText(newValue.getFirstName());
                    labelLastNameUser.setText(newValue.getSecondName());

                }
                else{
                   labelLastNameUser.setText("Last Name");
                   labelFirstNameUser.setText("First Name");

                }
            }
        });
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                String firstName = labelFirstNameUser.getText();
                String lastName = labelLastNameUser.getText();
                try{
                    Long id1 = service.findUserByFirstName(firstName,lastName);
                    srvRequest.sendFriendRequest(mainApp.getUserID(), id1);
                    labelFirstNameUser.setText("First Name");
                    labelLastNameUser.setText("Last Name");
                    showSFriendshipRequestSent();

                }catch (Exception e){
                    showErrorPopUp(e.getMessage());
                }
            }
        };
        friendRequestButton.setOnAction(event);
        EventHandler<ActionEvent> eventBackButton = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main-app.fxml"));
                    fxmlLoader.setController(mainApp);
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("MainMenu");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            };

        backButton.setOnAction(eventBackButton);
    }
    public void onNextButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasNextPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()+1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        nextButton.setOnAction(eventHandler);
    }
    public void onPrevButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasPreviousPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()-1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        prevButton.setOnAction(eventHandler);
    }
    public void setService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username, password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        service = new Service(repoUser,repoFriend);

    }
    public void setSrvRequest(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        FriendshipRequestDatabase repoFriendRequests = new FriendshipRequestDatabase(url, username, password, new FriendshipRequestValidator());
        srvRequest = new ServiceFriendshipRequest(repoFriendRequests, service) ;
    }
    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();

    }
    public static void showSFriendshipRequestSent(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Friendship request");
        alert.setHeaderText("Friendship request sent successfully!");
        alert.setContentText("The friendship request has been sent!\nYou need to wait for confirmation before you can start talking to the user!");
        alert.show();
    }

    @Override
    public void update() {
    }
    public void loadTablePage(){

        users.setAll(usersPagination.getItemsOnCurrentPage());
        nextButton.setDisable(!this.usersPagination.hasNextPage());
        prevButton.setDisable(!this.usersPagination.hasPreviousPage());
    }
    private void refreshFriendList(){
        List<Users> usersList = service.getAllUsersNotFriendsWithUser(mainApp.getUserID());
        usersPagination = new Pagination<>(usersList,pageSize,paginationSize);
        loadTablePage();
    }
    public void handleFilter(){

        Predicate<Users> p1 = n->n.getFirstName().startsWith(textFieldFirstName.getText());
        Predicate<Users> p2 = n->n.getSecondName().startsWith(textFieldLastName.getText());
        users.setAll(service.getAllUsersNotFriendsWithUser(mainApp.getUserID()).stream().filter(p1.and(p2)).collect(Collectors.toList()));
    }
}
