package com.example.map225theodorrobertgui.service;

import com.example.map225theodorrobertgui.MainApp;
import com.example.map225theodorrobertgui.domain.Event;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class Notification{
    public static boolean stopNotifications = false;
    public static int sNotificationFrquency = 60;
    private static ServiceEvent serviceEvent;
    private long userId;

    public Notification(ServiceEvent serviceEvent, long userId){
        this.serviceEvent = serviceEvent;
        this.userId = userId;
        activate();
    }

    public void activate(){
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(sNotificationFrquency), ev -> {
            for(String s : serviceEvent.getUserNotifications(userId)){
                Notifications.create()
                        .title("Event")
                        .text(s)
                        .showInformation();
            }
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
