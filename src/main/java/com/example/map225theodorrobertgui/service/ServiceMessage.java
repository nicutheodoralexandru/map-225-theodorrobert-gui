package com.example.map225theodorrobertgui.service;

import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.RepoException;
import com.example.map225theodorrobertgui.repository.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceMessage {
    private Repository<Long, Message> repoMessage;
    private Repository<Long, Users> usersRepository;
    private Repository<Long, Friendship> friendshipRepository;



    public ServiceMessage(Repository<Long, Message> repoMessage, Repository<Long, Users> usersRepository, Repository<Long, Friendship> friendshipRepository) {
        this.repoMessage = repoMessage;
        this.usersRepository = usersRepository;
        this.friendshipRepository = friendshipRepository;
    }






    public void sendMessage(Long id, String id1, String message){
        String[] parts = id1.split(";");
        for(String part: parts) {
            Long id2 = Long.parseLong(part);
            if (usersRepository.findOne(id) == null || usersRepository.findOne(id2) == null)
                throw new RepoException("Users not found!\n");
            if (friendshipRepository.findOne(new Friendship(id, id2, LocalDate.now())) == null && friendshipRepository.findOne(new Friendship(id2, id, LocalDate.now())) == null)
                throw new RepoException("You can't send a message to a user that you are not friends with!\n");
        }
        Message message1 = new Message(id,id1,message, LocalDateTime.now(),0L);
        repoMessage.save(message1);
    }

    public void deleteMessage(Long id,String id1, String message){
        String[] parts = id1.split(";");
        for(String part: parts) {
            Long id2 = Long.parseLong(part);
            if (usersRepository.findOne(id) == null || usersRepository.findOne(id2) == null)
                throw new RepoException("Users not found!\n");
            if (friendshipRepository.findOne(new Friendship(id, id2, LocalDate.now())) == null && friendshipRepository.findOne(new Friendship(id2, id, LocalDate.now())) == null)
                throw new RepoException("You can't send a message to a user that you are not friends with!\n");
        }
        Message message1 = new Message(id,id1,message, LocalDateTime.now(),null);
        if(repoMessage.findOne(message1) == null)
            throw new RepoException("This message doesn't exist!\n");
        repoMessage.remove(message1);
    }

    public void updateMessage(Long id,String id1,String message){
        String[] parts = id1.split(";");
        for(String part: parts) {
            Long id2 = Long.parseLong(part);
            if (usersRepository.findOne(id) == null || usersRepository.findOne(id2) == null)
                throw new RepoException("Users not found!\n");
            if (friendshipRepository.findOne(new Friendship(id, id2, LocalDate.now())) == null && friendshipRepository.findOne(new Friendship(id2, id, LocalDate.now())) == null)
                throw new RepoException("You can't send a message to a user that you are not friends with!\n");
        }
        Message message1 = new Message(id,id1,message, LocalDateTime.now(),null);
        if(repoMessage.findOne(message1) == null)
            throw new RepoException("This message doesn't exist!\n");
        repoMessage.update(message1);
    }

    public Iterable<Message> getAllMessages(){
        return repoMessage.findAll();
    }

    public List<Message> getMessagesUser(String id){
        Iterable<Message> messages = getAllMessages();
        List<Message> userMessages = new ArrayList<>();
        for(Message m: messages){
                if(m.getTo().contains(id))
                    userMessages.add(m);
        }
        return userMessages;
    }

    public void replyMessage(Long id,Long id1, String message,Long reply){
        if(usersRepository.findOne(id)==null || usersRepository.findOne(id1)==null)
            throw new RepoException("Users not found!\n");
        if(friendshipRepository.findOne(new Friendship(id,id1, LocalDate.now())) == null && friendshipRepository.findOne(new Friendship(id1,id, LocalDate.now())) == null)
            throw new RepoException("You can't send a message to a user that you are not friends with!\n");
        if(repoMessage.findOne(reply) == null)
            throw new RepoException("This message does not exist!\n");
        Message message1 = new Message(id,id1.toString(),message, LocalDateTime.now(),reply);
        repoMessage.save(message1);
    }

    public void replyAll(Long id,String id1,String message,Long reply){
        String[] parts = id1.split(";");
        for(String part: parts) {

            Long id2 = Long.parseLong(part);
            if (usersRepository.findOne(id) == null || usersRepository.findOne(id2) == null)
                throw new RepoException("Users not found!\n");
            if (friendshipRepository.findOne(new Friendship(id, id2, LocalDate.now())) == null && friendshipRepository.findOne(new Friendship(id2, id, LocalDate.now())) == null)
                throw new RepoException("You can't send a message to a user that you are not friends with!\n");
        }
        Message message1 = new Message(id,id1,message,LocalDateTime.now(),reply);
        repoMessage.save(message1);
    }

    public Users findUserById(Long id){
        return usersRepository.findOne(id);
    }

    public Message getMessageById(Long id){
        if(repoMessage.findOne(id) == null)
            throw new RepoException("Message does not exist!\n");
        return repoMessage.findOne(id);
    }


    public List<Message> getConversation(String id, String id1){
        List<Message> user1 = getMessagesUser(id);
        Collections.sort(user1, Comparator.comparing(Message::getDate));

        List<Message> user2 = getMessagesUser(id1);
        Collections.sort(user2,Comparator.comparing(Message::getDate));
        List<Message> user1Messages = user1.stream().filter(x-> x.getFrom() == Long.parseLong(id1)).collect(Collectors.toList());
        List<Message> user2Messages = user2.stream().filter(x->x.getFrom() ==Long.parseLong(id)).collect(Collectors.toList());
        int i=0,j=0;
        List<Message> conversation= new ArrayList<>();
        while(i<user1Messages.size()&&j<user2Messages.size()) {
            if(user1Messages.get(i).getDate().isBefore(user2Messages.get(j).getDate())){
                conversation.add(user1Messages.get(i));
                i++;
            }
            else {
                conversation.add(user2Messages.get(j));
                j++;
            }

        }
        while(i<user1Messages.size()) {
            conversation.add(user1Messages.get(i));
            i++;
        }
        while(j<user2Messages.size()) {
            conversation.add(user2Messages.get(j));
            j++;
        }
        return conversation;
    }




}


