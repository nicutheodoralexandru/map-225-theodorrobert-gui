package com.example.map225theodorrobertgui.service;

import com.example.map225theodorrobertgui.domain.FriendshipRequest;
import com.example.map225theodorrobertgui.domain.FriendshipRequestUI;
import com.example.map225theodorrobertgui.domain.Pair;
import com.example.map225theodorrobertgui.observer.Observable;
import com.example.map225theodorrobertgui.observer.Observer;
import com.example.map225theodorrobertgui.repository.database.FriendshipRequestDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceFriendshipRequest extends Observable {
    private final FriendshipRequestDatabase repo;
    private Service service;

    public ServiceFriendshipRequest(FriendshipRequestDatabase repo, Service service){
        this.repo = repo;
        this.service = service;
    }

    public void cancelFriendRequest(Long from, Long to){
        final Long[] id = {-1L};
        StreamSupport.stream(repo.findAll().spliterator(), false)
                .forEach(x -> { if(x.getPair().getFirst() == from && x.getPair().getSecond() == to) id[0] = x.getId(); } );
        if(id[0] == -1L)
            return;
        repo.remove(id[0]);
    }

    public void removeRequest(Long fromId, Long toId) throws Exception{
        repo.remove(
            getFriendRequestsFrom(fromId)
                    .stream()
                    .filter(x->x.getPair().getSecond() == toId)
                    .collect(Collectors.toList())
                        .get(0)
                        .getId());
    }

    public List<FriendshipRequest> getAll(){
        return StreamSupport.stream(repo.findAll().spliterator(), false).toList();
    }

    public void rejectRequest(Long id){
        FriendshipRequest f = repo.findOne(id);
        if(f.getStatus() != "pending")
            return;
        f.setStatus("rejected");
        repo.update(f);
        notifyObservers();
    }

    public void approveRequest(Long id){
        FriendshipRequest f = repo.findOne(id);
        if(f.getStatus() != "pending")
            return;
        f.setStatus("approved");
        repo.update(f);
        notifyObservers();
    }

    public List<FriendshipRequestUI> getFriendRequestsUITo(Long id){
        List<FriendshipRequestUI> friendshipRequestUIS = new ArrayList<>();
        for(FriendshipRequest r : getFriendRequestsTo(id)){
            Pair<String> pair = new Pair<>(service.findUser(r.getPair().getFirst()).getSecondName() + service.findUser(r.getPair().getFirst()).getFirstName(),
                    service.findUser(r.getPair().getSecond()).getSecondName() + service.findUser(r.getPair().getSecond()).getFirstName());
            friendshipRequestUIS.add(new FriendshipRequestUI(r, pair));
        }
        return friendshipRequestUIS;
    }

    public List<FriendshipRequest> getFriendRequestsTo(Long id){
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(x->x.getPair().getSecond().equals(id))
                .collect(Collectors.toList());
    }

    public List<FriendshipRequestUI> getFriendRequestsUIFrom(Long id){
        List<FriendshipRequestUI> friendshipRequestUIS = new ArrayList<>();
        for(FriendshipRequest r : getFriendRequestsFrom(id)){
            Pair<String> pair = new Pair<>(service.findUser(r.getPair().getFirst()).getSecondName() + service.findUser(r.getPair().getFirst()).getFirstName(),
                                            service.findUser(r.getPair().getSecond()).getSecondName() + service.findUser(r.getPair().getSecond()).getFirstName());
            friendshipRequestUIS.add(new FriendshipRequestUI(r, pair));
        }
        return friendshipRequestUIS;
    }

    public List<FriendshipRequest> getFriendRequestsFrom(Long id){
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(x->x.getPair().getFirst().equals(id))
                .collect(Collectors.toList());
    }

    public void sendFriendRequest(Long fromId, Long toId) throws Exception {
        AtomicBoolean unique = new AtomicBoolean(true);
        StreamSupport.stream(repo.findAll().spliterator(), false).forEach(x -> {
            if ((x.getPair().getFirst() == fromId && x.getPair().getSecond() == toId) ||
                    x.getPair().getFirst() == toId && x.getPair().getSecond() == fromId)
                unique.set(false);
        });
        if(!unique.get())
            throw new Exception("Exista deja o cerere de prietenie intre voi!");
        FriendshipRequest r = new FriendshipRequest(fromId, toId);
        repo.save(r);
        notifyObservers();
    }
}
