package com.example.map225theodorrobertgui.service;

import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Tuple;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.RepoException;
import com.example.map225theodorrobertgui.hashing.PasswordHashing;
import com.example.map225theodorrobertgui.observer.Observable;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.utils.Communities;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Service extends Observable {

    private Repository<Long, Users> userRepo;
    private Repository<Long, Friendship> friendRepo;

    public Service(Repository userRepo, Repository friendRepo) {
        this.userRepo = userRepo;
        this.friendRepo = friendRepo;
    }

    public List<Friendship> getFriendshipsOfUserId(long userId){
        return StreamSupport.stream(friendRepo.findAll().spliterator(), false)
                .filter(x->x.getPair().getFirst() == userId || x.getPair().getSecond() == userId)
                .collect(Collectors.toList());
    }

    /**
     * Function that adds a user to the repository
     * @param firstName: the first name of the user
     * @param lastName: the last name of the user
     * throws RepoException if the user already exists
     * throws ValidationException if the first or last name of the user is an empty string
     */
    public void addUser(String firstName, String lastName,String userName,String password){
        Users user = new Users(firstName,lastName,userName);
        if(userRepo.findOne(user) != null)
            throw new RepoException("User already exists!\n");
       userRepo.save(user);
       PasswordHashing passwordHashing = new PasswordHashing();
        try{
            String hashedPassword = passwordHashing.hashPassword(password);
            userRepo.setUserPassword(user,hashedPassword);
            notifyObservers();
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }

    }

    /**
     * Function that adds a friendship to the repository
     * @param id: id of the first user in the friendship
     * @param id1: id of the second user in the friendship
     *           throws RepoException if one of the users doesn't exist
     *           throws RepoException if one the friendship already exists
     *           throws Validation exception if one of the ids is negative or if the ids are equal
     *
     */
    public void addFriendship(Long id,Long id1) throws Exception {
        LocalDate date = LocalDate.now();
        Friendship friendship = new Friendship(id,id1,date);
        if(userRepo.findOne(id) == null || userRepo.findOne(id1) == null)
            throw new RepoException("Users not found!\n");
        if(friendRepo.findOne(friendship) != null)
            throw new RepoException("Friendship already exists!\n");
        AtomicBoolean unique = new AtomicBoolean(true);
        StreamSupport.stream(friendRepo.findAll().spliterator(), false).forEach(x -> {
            if ((x.getPair().getFirst() == id && x.getPair().getSecond() == id1) ||
                    x.getPair().getFirst() == id1 && x.getPair().getSecond() == id)
                unique.set(false);
        });
        if(!unique.get())
            throw new Exception();
        friendRepo.save(friendship);
        notifyObservers();
    }

    /**
     * Function that removes a user from the repository and removes all the users friendships in the process
     * @param id: the id of the user to be removed
     *          throws RepoException if the user doesn't exist
     */
    public void removeUser(Long id){
        if(userRepo.findOne(id) == null)
            throw new RepoException("User does not exist!\n");
        userRepo.remove(id);
        List<Friendship> toDeleteList = new ArrayList<>();
        for(Friendship friendship: friendRepo.findAll()){
            if(friendship.getPair().getSecond().equals(id)  || friendship.getPair().getFirst().equals(id))
                toDeleteList.add(friendship);
        }
        for(Friendship elem: toDeleteList){
            friendRepo.remove(elem);
        }
        notifyObservers();
    }

    /**
     * Function that removes a friendship from the repository
     * @param id: id of the first user
     * @param id1: id of the second user
     *           throws RepoException if the friendship doesn't exist
     */
    public void removeFriendship(Long id, Long id1){
        Friendship friendship = new Friendship(id,id1,getDate(id,id1));
        if(friendRepo.findOne(friendship) == null)
            throw new RepoException("Friendship does not exist!\n");
        friendRepo.remove(friendship);
        notifyObservers();
    }
    public void updateUser(Long id, String firstName,String lastName,String userName){
        Users user = new Users(firstName,lastName,userName);
        user.setId(id);
        if(userRepo.findOne(id) == null)
            throw new RepoException("User does not exist!\n");
        userRepo.update(user);
        notifyObservers();
    }

    /**
     * Function that returns all users from the repository of users
     * @return
     */
    public Users findOne(Long id){
        if(userRepo.findOne(id) == null)
            throw new RepoException("User does not exist!\n");
        return userRepo.findOne(id);
    }
    public Iterable<Users> getAllUsers(){
        return userRepo.findAll();
    }
    public String getUsernames(String id){
        String[] parts = id.split(";");
        String usernames = "";
        for(String part : parts){
            usernames+= findOne(Long.parseLong(part)).getUserName()+";";
        }
        String usernamesFinal = usernames.substring(0,usernames.length()-1);
        return usernamesFinal;
    }

    /**
     * Function that returns all friendships from the repository of friendships
     * @return
     */
    public Iterable<Friendship> getAllFriendships(){
        return friendRepo.findAll();
    }

    /**
     * Function that returns all the friends of a user with a sepcific id
     * @param id: the id of the user
     * @return
     */
    public List<Tuple<Users,LocalDate>> getAllFriendsUser(Long id) {
        Iterable<Friendship> friendships = friendRepo.findAll();
        List<Tuple<Users,LocalDate>> userFriends = new ArrayList<>();
        for (Friendship friendship : friendships) {
            if (friendship.getPair().getFirst().equals(id)) {
                Tuple<Users,LocalDate> usersTuple = new Tuple<>(userRepo.findOne(friendship.getPair().getSecond()),friendship.getFriendshipDate());
                userFriends.add(usersTuple);
            }
            else
                if(friendship.getPair().getSecond().equals(id)){
                    Tuple<Users,LocalDate> usersTuple = new Tuple<>(userRepo.findOne(friendship.getPair().getFirst()),friendship.getFriendshipDate());
                    userFriends.add(usersTuple);
                }
        }
        return userFriends;
    }

    /**
     * Function that returns the number of communities based on friendships between users
     * @return
     */
    public int nrCommunities(){
        Communities communities = new Communities(userRepo,friendRepo);
        return communities.nrOfCommunities();
    }

    /**
     * Function that returns the communities of users in the application
     * @return
     */
    public Map<Integer,List<Users>> getCommunities(){
        Communities communities = new Communities(userRepo,friendRepo);
        return communities.getAllCommunities(userRepo);
    }

    /**
     * Function that returns the most social community
     * @return
     */
    public int mostSocial(){
        Communities communities = new Communities(userRepo,friendRepo);
        return communities.mostSocialCommunity();
    }
    public LocalDate getDate(Long id,Long id1){
        Iterable<Friendship> friendships = getAllFriendships();
        for(Friendship f: friendships){
            if(f.getPair().getFirst()==id && f.getPair().getSecond()==id1 || f.getPair().getFirst()==id1 && f.getPair().getSecond()==id)
                return f.getFriendshipDate();
        }
        return null;
    }
    public Long findUser(String userName, String password){
        Iterable<Users> users = getAllUsers();
        String hashedPassword;
        PasswordHashing hashing = new PasswordHashing();
        for(Users u: users){
            try{
                hashedPassword = hashing.hashPassword(password);
                if(u.getUserName().equals(userName) && hashedPassword.equals(userRepo.getPassword(u)))
                    return u.getId();
            }catch (NoSuchAlgorithmException e){
                e.printStackTrace();
            }
        }
        throw new RepoException("User does not exist!\n");
    }
    public Long findUserByFirstName(String firstName, String lastName){
        Iterable<Users> users = getAllUsers();
        for(Users u: users){
            if(u.getFirstName().equals(firstName) && u.getSecondName().equals(lastName))
                return u.getId();
        }
        throw new RepoException("User does not exist!\n");
    }
    public List<Tuple<Users,LocalDate>> getUserFriendsDate(Long id,int month){
        List<Tuple<Users,LocalDate>> usersList = getAllFriendsUser(id);
        List<Tuple<Users,LocalDate>> filteredUsersList =
                usersList.stream().filter(x->x.getRight().getMonthValue() == month).collect(Collectors.toList());

        return filteredUsersList;
    }

    public Users findUser(long id) {
        return userRepo.findOne(id);
    }

    public List<Users> getAllUsersNotFriendsWithUser(Long id){
        List<Users> users = new ArrayList<>();
        Iterable<Users> usersIterable = getAllUsers();
        for(Users user: usersIterable){
            if(user.getId() != id) {
                Friendship friendship = new Friendship(id, user.getId(), getDate(id, user.getId()));

                if (friendRepo.findOne(friendship) == null) {
                    users.add(user);
                }
            }
        }

        return users;
    }

}
