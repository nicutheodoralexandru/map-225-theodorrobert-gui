package com.example.map225theodorrobertgui.service;

import com.example.map225theodorrobertgui.domain.Event;
import com.example.map225theodorrobertgui.domain.EventUI;
import com.example.map225theodorrobertgui.observer.Observable;
import com.example.map225theodorrobertgui.repository.database.EventDatabase;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceEvent extends Observable {
    private final EventDatabase repo;
    private Service service;

    public ServiceEvent(EventDatabase repo, Service service) {
        this.repo = repo;
        this.service = service;
    }

    public void createNewEvent(long userId, LocalDate date, String location){
        repo.save(new Event(userId, date, location));
    }

    public List<String> getUserNotifications(long userId)
    {
        final long id = userId;
        final List<String> notifications = new ArrayList<>();
        final LocalDateTime now = LocalDateTime.now();
        StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(x -> x.getAtendees().contains(String.valueOf(id) + ";"))
                .filter(x -> x.getDate().isAfter(now.toLocalDate()) || x.getDate().equals(now.toLocalDate()))
                .forEach(x -> notifications.add("Pe data de " + x.getDate().toString() + " are loc evenimentul " + x.getLocation()));
        return notifications;
    }

    public void cancelEvent(long eventId, long userId) throws Exception {
        Event e = repo.findOne(eventId);
        String s1 = String.valueOf(e.getOrganizerId());
        String s2 = String.valueOf(userId);
        if( !s1.equals(s2) )
            throw new Exception("Doar organizatorul poate anula un eveniment!");
        repo.remove(eventId);
    }

    public void removeAtendeee(long eventId, long atendeeId) throws Exception {
        Event e = repo.findOne(eventId);
        if(!e.getAtendees().contains(String.valueOf(atendeeId) + ";"))
            throw new Exception("Acest utilizator nu participa la acest eveniment!");
        String s = e.getAtendees().replace(String.valueOf(atendeeId) + ";", "");
        e.setAtendees(s);
        repo.update(e);
    }

    public void addAtendee(long eventId, long atendeeId) throws Exception {
        Event e = repo.findOne(eventId);
        if(e.getAtendees().contains(String.valueOf(atendeeId) + ";"))
            throw new Exception("Acest utilizator participa deja la acest eveniment!");
        e.addAtendee(atendeeId);
        repo.update(e);
    }

    public List<EventUI> getUserEventsUI(long userId) {
        List<EventUI> eventUIS = new ArrayList<>();
        for(Event e : getUserEvents(userId)){
            String organizerName = service.findUser(e.getOrganizerId()).getSecondName() + " " + service.findUser(e.getOrganizerId()).getFirstName();
            String atendeesName = "";
            for(String id : e.getAtendees().split(";")){
                if(id == "")
                    continue;
                atendeesName += service.findUser(Long.parseLong(id)).getSecondName() + " " + service.findUser(Long.parseLong(id)).getFirstName() + ",";
            }
            eventUIS.add(new EventUI(e, organizerName, atendeesName));
        }
        return eventUIS;
    }

    public List<Event> getUserEvents(long userId){
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(x -> x.getAtendees().contains(String.valueOf(userId) + ";"))
                .collect(Collectors.toList());
    }
}
