package com.example.map225theodorrobertgui.service;

import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Pair;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1CFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceReports {
    private Repository<Long, Message> repoMessage;
    private Repository<Long, Friendship> repoFriendship;
    private Repository<Long, Users> repoUser;

    public ServiceReports(Repository<Long, Message> repoMessage, Repository<Long, Friendship> repoFriendship, Repository<Long, Users> repoUser){
        this.repoFriendship = repoFriendship;
        this.repoMessage = repoMessage;
        this.repoUser = repoUser;
    }

    public void generatePdfReport2(File file, long userId, long otherId, LocalDate startDate, LocalDate endDate) throws IOException {
        List<Message> messages = StreamSupport.stream(repoMessage.findAll().spliterator(), false)
                .filter(x -> x.getTo().contains(String.valueOf(userId)))
                .filter(x -> x.getFrom() == otherId)
                .filter(x -> x.getDate().toLocalDate().isAfter(startDate) && x.getDate().toLocalDate().isBefore(endDate))
                .collect(Collectors.toList());
        //save to pdf
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);
        PDPageContentStream contentStream = new PDPageContentStream(document, page);
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
        contentStream.setLeading(14.5f);
        contentStream.beginText();
        PDPageContentStream finalContentStream = contentStream;
        finalContentStream.newLineAtOffset(50, 750);
        int offset = 25;
        messages.stream().forEach(x -> {
            try {
                finalContentStream.showText(x.toString());
                finalContentStream.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        contentStream.endText();
        contentStream.close();
        document.save(file.getAbsolutePath());
        document.close();
    }



    public void generatePdfReport1(File file, long userId, LocalDate startDate, LocalDate endDate) throws IOException {
        //get all new friends
        List<Users> newFriends = new ArrayList<>();
        List<Friendship> newFriendships = StreamSupport.stream(repoFriendship.findAll().spliterator(), false)
                .filter(x -> x.getFriendshipDate().isAfter(startDate) && x.getFriendshipDate().isBefore(endDate))
                .collect(Collectors.toList());
        newFriendships.stream().forEach(x -> {
            long otherId;
            Pair<Long> ids = x.getPair();
            otherId = ids.getFirst();
            if(userId == otherId)
                otherId = ids.getSecond();
            newFriends.add(repoUser.findOne(otherId));
        });
        //get all received messages
        List<Message> newMessages = StreamSupport.stream(repoMessage.findAll().spliterator(), false)
                .filter(x -> x.getTo().contains(String.valueOf(userId)) &&
                                x.getDate().toLocalDate().isAfter(startDate) &&
                                x.getDate().toLocalDate().isBefore(endDate)
                )
                .collect(Collectors.toList());
        //save to pdf
        PDDocument document = new PDDocument();
        PDPage pageNewFriends = new PDPage();
        PDPage pageNewMessages = new PDPage();
        document.addPage(pageNewMessages);
        document.addPage(pageNewFriends);
        PDPageContentStream contentStream = new PDPageContentStream(document, pageNewFriends);
        contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
        contentStream.setLeading(14.5f);
        contentStream.beginText();
        PDPageContentStream finalContentStream = contentStream;
        finalContentStream.newLineAtOffset(50, 750);
        int offset = 25;
        newFriends.stream().forEach(x -> {
            try {
                finalContentStream.showText(x.toString());
                finalContentStream.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        contentStream.endText();
        contentStream.close();
        PDPageContentStream contentStream2 = new PDPageContentStream(document, pageNewMessages);
        contentStream2.setFont(PDType1Font.TIMES_ROMAN, 12);
        contentStream2.setLeading(14.5f);
        contentStream2.beginText();
        PDPageContentStream finalContentStream2 = contentStream2;
        finalContentStream2.newLineAtOffset(50, 750);
        newMessages.stream().forEach(x -> {
            try {
                System.out.println(x.toString());
                finalContentStream2.showText(x.toString());
                finalContentStream2.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        contentStream2.endText();
        contentStream2.close();
        document.save(file.getAbsolutePath());
        document.close();
    }
}
