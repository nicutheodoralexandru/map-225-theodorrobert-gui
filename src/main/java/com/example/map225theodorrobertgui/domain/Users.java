package com.example.map225theodorrobertgui.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Users extends Entity<Long> {

    private static Long idCurrent = 1L;
    private String FirstName;
    private String SecondName;
    private String userName;

    /**
     * Constructor of class Users
     * @param firstName: type String representing the first name of the user
     * @param secondName: type String representing the second name of the user
     */
    public Users(String firstName, String secondName, String userName) {
        FirstName = firstName;
        SecondName = secondName;
        this.userName = userName;
        this.setId(System.nanoTime());
    }

    /**
     * Function that returns the first name of the user
     * @return String representing the first name of the User
     */
    public String getFirstName() {
        return FirstName;
    }

    public String getUserName() {
        return userName;
    }

    /**
     * Function that returns the second name of the user
     * @return String representing the second name of the User
     */
    public String getSecondName() {
        return SecondName;
    }

    @Override

    public String toString() {
        return "Users{" +
                "id=" + id +
                ", FirstName='" + FirstName + '\'' +
                ", SecondName='" + SecondName + '\'' +
                '}';
    }

    @Override

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Users users = (Users) o;
        return this.getUserName().equals(((Users) o).userName);
    }

    @Override

    public int hashCode() {
        return Objects.hash(this.getId());
    }
}
