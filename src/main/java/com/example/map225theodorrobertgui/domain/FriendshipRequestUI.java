package com.example.map225theodorrobertgui.domain;

import java.time.LocalDate;
import java.util.Objects;

public class FriendshipRequestUI extends Entity<Long>{
    private final Pair<Long> pair;
    private String status;
    private static Long idCurrent = System.nanoTime();
    private LocalDate date;
    private Pair<String> pairNames;

    public FriendshipRequestUI(FriendshipRequest r, Pair<String> pairNames){
        this.pairNames = pairNames;
        this.status = r.getStatus();
        this.date = r.getDate();
        this.setId(r.getId());
        this.pair = r.getPair();
    }

    public Pair<String> getPairNames() {
        return pairNames;
    }

    public FriendshipRequestUI(Long first, Long second) {
        this(first, second, "pending");
    }

    public FriendshipRequestUI(Long first, Long second, String status) {
        this(idCurrent++, first, second, status, LocalDate.now());
    }

    public FriendshipRequestUI(Long id, Long first, Long second, String status, LocalDate date){
        pair = new Pair<>(first, second);
        this.status = status;
        super.id = id;
        this.date = date;
        this.setId(id);
    }

    public LocalDate getDate() {
        return date;
    }

    public Pair<Long> getPair() {
        return pair;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "FriendshipRequest{" +
                "id=" + id +
                ", pair=" + pair.getFirst() + " "+
                pair.getSecond() +
                ", status=" + status +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if(!this.date.isEqual(((FriendshipRequestUI) o).getDate())) return false;
        //if (!super.equals(o)) return false;
        FriendshipRequestUI that = (FriendshipRequestUI) o;
        return pair.equals(that.pair) && status.equals(that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pair, status);
    }
}
