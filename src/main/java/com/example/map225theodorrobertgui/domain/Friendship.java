package com.example.map225theodorrobertgui.domain;

import java.time.LocalDate;
import java.util.*;


public class Friendship extends Entity<Long> {

    private Pair<Long> pair;
    private LocalDate friendshipDate;
    private static Long idCurrent = 1L;

    public Friendship(Long first, Long second,LocalDate friendshipDate) {
        pair = new Pair<>(first, second);
        this.friendshipDate=friendshipDate;
        this.setId(System.nanoTime());
    }

    public Pair<Long> getPair() {
        return pair;
    }

    public LocalDate getFriendshipDate() {
        return friendshipDate;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id=" + id +
                ", pair=" + pair.getFirst() + " "+
                pair.getSecond() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //if (!super.equals(o)) return false;
        Friendship that = (Friendship) o;
        return pair.equals(that.pair);
    }

    @Override
    public int hashCode() {
        return Objects.hash("x");
    }
}
