package com.example.map225theodorrobertgui.domain;

import java.time.LocalDateTime;
import java.util.List;

public class Message extends Entity<Long>{
    private static Long idCurrent = 1L;
    private Long from;
    private String to;
    private String message;
    private LocalDateTime date;
    private Long reply;

    public Message(Long from, String to, String message, LocalDateTime date, Long reply) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.reply = reply;
        this.setId(System.nanoTime());
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to='" + to + '\'' +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", reply=" + reply +
                '}';
    }

    public static Long getIdCurrent() {
        return idCurrent;
    }

    public static void setIdCurrent(Long idCurrent) {
        Message.idCurrent = idCurrent;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getReply() {
        return reply;
    }

    public void setReply(Long reply) {
        this.reply = reply;
    }



}
