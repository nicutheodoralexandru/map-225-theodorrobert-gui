package com.example.map225theodorrobertgui.domain.validators;


import com.example.map225theodorrobertgui.domain.Message;

public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws ValidationException {
        String errors = "";
        if(entity.getTo().equals(""))
            errors += "The id of the receiver is invalid!\n";
        if(entity.getFrom()<0)
            errors += "The id of the sender is invalid!\n";
        if(entity.getMessage().equals(""))
            errors += "The message can't be blank!\n";
        if(!errors.equals(""))
            throw  new ValidationException(errors);
    }
}
