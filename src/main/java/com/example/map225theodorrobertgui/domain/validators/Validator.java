package com.example.map225theodorrobertgui.domain.validators;

public interface Validator<T> {
    /**
     * Function that validates a generic object of type T
     * @param entity: generic object of type T
     * @throws ValidationException if the object is not valid
     */
    void validate(T entity)throws ValidationException;
}
