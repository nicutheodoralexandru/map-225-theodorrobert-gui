package com.example.map225theodorrobertgui.domain.validators;

import com.example.map225theodorrobertgui.domain.Event;
import com.example.map225theodorrobertgui.domain.FriendshipRequest;

public class EventValidator implements Validator<Event>{
    /**
     * Function that validates an object of type Friendship
     * @param entity:object of type Friendship
     * @throws ValidationException if one of the ids in the pair is negative or if the two ids are equal
     */
    @Override
    public void validate(Event entity) throws ValidationException {
        String errors = "";
        if(entity.getLocation() == "")
            errors += "Invalid location!\n";
        if(entity.getDate() == null)
            errors += "Please select a date!\n";
        if(!errors.equals(""))
            throw new ValidationException(errors);
    }
}
