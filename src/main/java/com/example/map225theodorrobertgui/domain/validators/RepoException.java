package com.example.map225theodorrobertgui.domain.validators;

public class RepoException extends RuntimeException{
    public RepoException(){
    }
    public RepoException(String message){
        super(message);
    }
}
