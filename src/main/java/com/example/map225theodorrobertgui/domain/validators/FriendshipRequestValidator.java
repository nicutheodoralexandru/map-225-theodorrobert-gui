package com.example.map225theodorrobertgui.domain.validators;

import com.example.map225theodorrobertgui.domain.FriendshipRequest;

public class FriendshipRequestValidator implements Validator<FriendshipRequest>{
    /**
     * Function that validates an object of type Friendship
     * @param entity:object of type Friendship
     * @throws ValidationException if one of the ids in the pair is negative or if the two ids are equal
     */
    @Override
    public void validate(FriendshipRequest entity) throws ValidationException {
        String errors = "";
        if(entity.getPair().getSecond().equals(entity.getPair().getFirst()))
            errors += "Invalid pair!\n Put two different ids!\n";
        if(entity.getPair().getFirst() <= 0 || entity.getPair().getSecond() <=0)
            errors += "The ids must be positive numbers!\n";
        if(!entity.getStatus().equals("approved") && !entity.getStatus().equals("pending") && !entity.getStatus().equals("rejected"))
            errors += "The status must be 'approved', 'pending' or 'rejected'\n";
        if(!errors.equals(""))
            throw new ValidationException(errors);
    }
}
