package com.example.map225theodorrobertgui.domain.validators;

import com.example.map225theodorrobertgui.domain.Users;

public class UserValidator implements Validator<Users> {

    /**
     * Function that validates a user
     * @param entity:object of type Users
     * @throws ValidationException if the users first or second name is an empty string
     */
    @Override
    public void validate(Users entity) throws ValidationException {
        Integer errors = 0;
        if(entity.getFirstName().equals(""))
            errors += 1;
        if(entity.getSecondName().equals(""))
            errors += 10;
        if(entity.getUserName().equals(""))
            errors += 100;
        if(errors != 0)
            throw new ValidationException(errors.toString());
    }
}
