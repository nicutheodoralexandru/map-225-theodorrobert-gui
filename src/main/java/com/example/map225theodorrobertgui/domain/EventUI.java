package com.example.map225theodorrobertgui.domain;

import java.time.LocalDate;
import java.util.Objects;

public class EventUI extends Entity<Long>{
    private String atendees;
    private long organizerId;
    private LocalDate date;
    private String location;
    private String organizerName;
    private String atendeesName;

    public EventUI(Event e, String organizerName, String atendeesName){
        this(e.getId(), e.getAtendees(), e.getOrganizerId(), e.getDate(), e.getLocation(), organizerName, atendeesName);
    }

    public EventUI(Long id, String atendees, long organizerId, LocalDate date, String location, String organizerName,
                   String atendeesName){
        super.id = id;
        this.organizerId = organizerId;
        this.date = date;
        this.location = location;
        this.atendees = atendees;
        this.organizerName = organizerName;
        this.atendeesName = atendeesName;
    }

    public String getOrganizerName() {
        return organizerName;
    }

    public String getAtendeesName() {
        return atendeesName;
    }

    public void setAtendees(String atendees) {
        this.atendees = atendees;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Event{" +
                "atendees='" + atendees + '\'' +
                ", organizerId=" + organizerId +
                ", date=" + date +
                ", location='" + location + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        EventUI event = (EventUI) o;
        return organizerId == event.organizerId && Objects.equals(atendees, event.atendees) && Objects.equals(date, event.date) && Objects.equals(location, event.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), atendees, organizerId, date, location);
    }

    public void addAtendee(long id){
        if(!atendees.contains(String.valueOf(id))){
            atendees += String.valueOf(id) + ";";
        }
        int a = 5;
    }

    public String getLocation() {
        return location;
    }

    public String getAtendees() {
        return String.copyValueOf(atendees.toCharArray());
    }

    public long getOrganizerId() {
        return organizerId;
    }

    public LocalDate getDate() {
        return date;
    }
}
