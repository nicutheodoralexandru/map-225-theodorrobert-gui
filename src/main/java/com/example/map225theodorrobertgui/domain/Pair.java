package com.example.map225theodorrobertgui.domain;

import java.io.Serializable;
import java.util.Objects;

public class Pair<ID> implements Serializable {
    protected ID first;
    protected ID second;

    /**
     * Constructor for the class Pair that creates a Pair of IDs
     * @param first: generic type ID representing the ID of the first User
     * @param second: generic type Id representing the ID of the second user
     */
    public Pair(ID first, ID second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Function that returns
     * @return
     */
    public ID getFirst() {
        return first;
    }

    public ID getSecond() {
        return second;
    }

    public void setFirst(ID first) {
        this.first = first;
    }

    public void setSecond(ID second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?> pair = (Pair<?>) o;
        return first.equals(pair.first) && second.equals(pair.second) || first.equals(pair.second) && second.equals(pair.first);
    }

    @Override
    public int hashCode() {
        return Objects.hash("x");
    }
}
