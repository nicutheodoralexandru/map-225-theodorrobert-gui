package com.example.map225theodorrobertgui.main;

import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.FriendshipRequestValidator;
import com.example.map225theodorrobertgui.domain.validators.FriendshipValidator;
import com.example.map225theodorrobertgui.domain.validators.MessageValidator;
import com.example.map225theodorrobertgui.domain.validators.UserValidator;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.FriendshipRequestDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceFriendshipRequest;
import com.example.map225theodorrobertgui.service.ServiceMessage;
import com.example.map225theodorrobertgui.ui.UI;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

public class Main {

    public static void main(String[] args) {
        /*UserValidator validatorUser = new UserValidator();
        FriendshipValidator friendshipValidator = new FriendshipValidator();
        if(args[0].equals("file")){
            UserFile userFile = new UserFile(validatorUser,"C:\\Users\\Robi\\IdeaProjects\\Lab3\\src\\com\\company\\users.txt");
            FriendshipFile friendshipFile = new FriendshipFile(friendshipValidator,"C:\\Users\\Robi\\IdeaProjects\\Lab3\\src\\com\\company\\friends.txt");
            Service srv = new Service(userFile,friendshipFile);
            UI ui = new UI(srv);
            ui.start();
        }
        else
        {
            InMemoryRepo repoUser = new InMemoryRepo(validatorUser);
            InMemoryRepo repoFriendship = new InMemoryRepo(friendshipValidator);
            Service srv = new Service(repoUser,repoFriendship);
            UI ui = new UI(srv);
            ui.start();
        }*/
        //load properties
        /*Properties properties = new Properties();
        ClassLoader test = Thread.currentThread().getContextClassLoader();

        InputStream stream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("resources/com.example.map225theodorrobertgui/mysql.properties");
        InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        try {
            properties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        //start db
        /*url = properties.getProperty("url");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        System.out.println("URL: " + url);
        Repository<Long, Users> repoUser = new UserDatabase(url, username, password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Repository<Long, Message> repoMessage = new MessageDatabase(url, username, password,new MessageValidator());
        FriendshipRequestDatabase db = new FriendshipRequestDatabase(url, username, password, new FriendshipRequestValidator());
        Service srv = new Service(repoUser,repoFriend);
        ServiceMessage srvMsg = new ServiceMessage(repoMessage,repoUser,repoFriend);
        ServiceFriendshipRequest srvF = new ServiceFriendshipRequest(db);
        UI ui = new UI(srv,srvMsg, srvF);
        ui.start();(*/
    }
}
