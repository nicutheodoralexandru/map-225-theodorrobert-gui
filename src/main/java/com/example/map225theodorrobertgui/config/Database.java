package com.example.map225theodorrobertgui.config;

public class Database {
    public static String url;
    public static String username;
    public static String password;

    public static void initMysql(){
        username="postgres";
        password="parola";
        url="jdbc:postgresql://localhost:5432/SocialNetwork";
    }

   public static void initPostresql()
   {
        url = "jdbc:postgresql://localhost:5432/SocialNetwork";
        username = "postgres";
        password = "parola";
    }
}
