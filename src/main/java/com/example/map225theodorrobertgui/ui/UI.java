package com.example.map225theodorrobertgui.ui;
//
//import com.example.map225theodorrobertgui.domain.*;
//import com.example.map225theodorrobertgui.domain.validators.RepoException;
//import com.example.map225theodorrobertgui.domain.validators.ValidationException;
//import com.example.map225theodorrobertgui.service.Service;
//import com.example.map225theodorrobertgui.service.ServiceFriendshipRequest;
//import com.example.map225theodorrobertgui.service.ServiceMessage;
//
//import java.lang.reflect.Field;
//import java.security.spec.ECField;
//import java.sql.SQLException;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.*;
//import java.util.function.Function;
//import java.util.stream.Collectors;
//
public class UI {
//    private Service service;
//    private ServiceFriendshipRequest serviceFriendshipRequest;
//    private ServiceMessage serviceMSG;
//    private Scanner scanner;
//    private Long idConnectedUser;
//
//    public UI(Service service, ServiceMessage serviceMSG, ServiceFriendshipRequest serviceFriendshipRequest) {
//        this.service = service;
//        this.serviceFriendshipRequest = serviceFriendshipRequest;
//        this.serviceMSG = serviceMSG;
//        scanner = new Scanner(System.in);
//        idConnectedUser = 0L;
//    }
//
//    private void addUserUI(){
//
//        System.out.println("Introduce first name: ");
//        String firstName = scanner.nextLine();
//        System.out.println("Introduce last name: ");
//        String lastName = scanner.nextLine();
//        System.out.println("Introduce username: ");
//        String userName = scanner.nextLine();
//        System.out.println("Introduce password: ");
//        String password = scanner.nextLine();
//
//        try{
//            service.addUser(firstName,lastName,userName,password);
//            System.out.println("User added!\n");
//        }catch(RepoException | ValidationException | IllegalArgumentException ex){
//            System.out.println(ex.getMessage());
//            return;
//        }
//
//    }
//    private void removeUserUI(){
//        System.out.println("Introduce the ID of the user you want removed: ");
//        Long id = null;
//        try{
//            id = Long.parseLong(scanner.nextLine());
//        }catch (NumberFormatException ex){
//            System.out.println("Id must be a number!\n");
//            return;
//        }
//        try{
//            service.removeUser(id);
//            System.out.println("User removed!\n");
//        }catch(RepoException  | IllegalArgumentException ex){
//            System.out.println(ex.getMessage());
//            return;
//        }
//
//    }
//    private void menu(){
//        System.out.println("Commands: ");
//        System.out.println("exit - exit the application");
//        System.out.println("menu - shows the commands that can be executed");
//        System.out.println("add user - adds a new user");
//        System.out.println("remove user - removes an existing user");
//        System.out.println("login - login into an account");
//        System.out.println("update user - updates an existing user");
//        System.out.println("show users - shows existing users");
//        System.out.println("show friends - shows existing friendships");
//        System.out.println("show communities - shows the number of communities and all communities");
//        System.out.println("most social - shows the most social community");
//        System.out.println("1. show user friends 2 - shows a specific users friends(now with stream!)");
//    }
//    private void addFriendshipUI(){
//
//        System.out.println("Introduce the second users ID: ");
//        Long id1 = null;
//        try{
//            id1= Long.parseLong(scanner.nextLine());
//        }catch (NumberFormatException ex){
//            System.out.println("Id must be a number!\n");
//            return;
//        }
//        try{
//            serviceFriendshipRequest.sendFriendRequest(idConnectedUser, id1);
//            System.out.println("Friendship request sent!\n");
//        }catch (RepoException | ValidationException | IllegalArgumentException ex)
//        {
//            System.out.println(ex.getMessage());
//            return;
//        }
//    }
//    private void removeFriendshipUI(){
//
//        System.out.println("Introduce the second users ID: ");
//        Long id1 = null;
//        try{
//            id1= Long.parseLong(scanner.nextLine());
//        }catch (NumberFormatException ex){
//            System.out.println("Id must be a number!\n");
//            return;
//        }
//        try{
//            serviceFriendshipRequest.removeRequest(idConnectedUser, id1);
//            service.removeFriendship(idConnectedUser,id1);
//            System.out.println("Friendship removed!\n");
//        }catch (RepoException | IllegalArgumentException ex)
//        {
//            System.out.println(ex.getMessage());
//            return;
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }
//    private void updateUserUI(){
//        System.out.println("Introduce the ID of the user you want to modify: ");
//        Long id = null;
//        try{
//            id = Long.parseLong(scanner.nextLine());
//        }catch (NumberFormatException ex){
//            System.out.println("Id must be a number!\n");
//            return;
//        }
//        System.out.println("Introduce the new first name: ");
//        String firstName = scanner.nextLine();
//        System.out.println("Introduce the new last name: ");
//        String lastName = scanner.nextLine();
//        System.out.println("Introduce the username: ");
//        String userName = scanner.nextLine();
//        try{
//            service.updateUser(id,firstName,lastName,userName);
//            System.out.println("User modified successfully!\n");
//        }catch (ValidationException|RepoException|IllegalArgumentException e){
//            System.out.println(e.getMessage());
//        }
//
//    }
//    private void getAllUsers(){
//       Iterable<Users> listUsers = service.getAllUsers();
//        System.out.println("Users:\n");
//       for(Users user: listUsers){
//           System.out.println("ID: "+user.getId() + "|" + "First name: " + user.getFirstName() + "|" + "Second name: " + user.getSecondName());
//
//       }
//        System.out.println("\n");
//    }
//    private void getAllFriendships(){
//        Iterable<Friendship> listFriendship = service.getAllFriendships();
//        System.out.println("Friendships:\n");
//        for(Friendship friendship: listFriendship){
//            System.out.println("First users ID: " + friendship.getPair().getFirst() + "|" + "Second users ID: " + friendship.getPair().getSecond());
//
//        }
//        System.out.println("\n");
//    }
//    private void getAllFriendsUser(Long id){
//
//        List<Tuple<Users, LocalDate>> usersFriends = service.getAllFriendsUser(id);
//        for(Tuple<Users,LocalDate> usersLocalDateTuple: usersFriends)
//        {
//            System.out.println("ID: "+usersLocalDateTuple.getLeft().getId() + "|" + "First name: " + usersLocalDateTuple.getLeft().getFirstName() + "|" + "Second name: " + usersLocalDateTuple.getLeft().getSecondName());
//        }
//    }
//    private void nrOfCommunities(){
//        System.out.println("There are " + service.nrCommunities() + " communities.");
//    }
//    private void getAllCommunities(){
//        Map<Integer,List<Users>> mapCommunities = service.getCommunities();
//        int nrCommunities = service.nrCommunities();
//        for(int i=1;i<=nrCommunities;i++){
//            System.out.println("Community number " + i);
//            for (Map.Entry<Integer, List<Users>> entry : mapCommunities.entrySet()){
//                if(entry.getKey() == i){
//                    for(Users user: entry.getValue())
//                        System.out.println("ID: "+user.getId() + "|" + "First name: " + user.getFirstName() + "|" + "Second name: " + user.getSecondName());
//                }
//            }
//        }
//    }
//    private void mostSocialCommunity(){
//        System.out.println("The most social community is community number " + service.mostSocial() + ".");
//        System.out.println("Congratulations!\n");
//    }
//    private void menuUser(){
//        System.out.println("Commands: ");
//        System.out.println("exit - logout");
//        System.out.println("menu - shows the commands that can be executed");
//        System.out.println("add friend - sends a friend request");
//        System.out.println("remove friend - deletes friendship");
//        System.out.println("show user friends - shows a specific users friends");
//        System.out.println("show friend requests");
//        System.out.println("approve friend request");
//        System.out.println("reject friend request");
//        System.out.println("1. show user friends 2 - shows a specific users friends(now with stream!)");
//        System.out.println("show user friends by month - shows the friendships formed in specific month");
//        System.out.println("send message - send a message to a friend");
//        System.out.println("reply - reply to a sent message");
//        System.out.println("see all messages - see all your messages");
//        System.out.println("delete message - deletes a selected message");
//        System.out.println("update message - updates a selected message");
//        System.out.println("see conversation - allows you to see a conversation with another user");
//
//    }
//
//    private void showFriendsByUserId(){
//        //read id
//        System.out.println("id: ");
//        String strId = scanner.nextLine();
//        long id = Long.parseLong(strId);
//        service.getFriendshipsOfUserId(id).stream().forEach((x) -> {
//            long otherId = x.getPair().getFirst();
//            if(otherId == id)
//                otherId = x.getPair().getSecond();
//            Users u = service.findUser(otherId);
//            System.out.println(u.getFirstName() + "|" + u.getSecondName() + "|" + x.getFriendshipDate().toString());
//        });
//    }
//
//    private void filterUserFriendsAfterMonth(Long id){
//        System.out.println("Introduce the month: ");
//        Integer month =0;
//        try{
//            month = Integer.parseInt(scanner.nextLine());
//        }catch (NumberFormatException ex) {
//            System.out.println("Month invalid!\nMust be a number!\n");
//            return;
//        }
//        if(month > 12 || month <1)
//        {
//            System.out.println("Month must be between 1 and 12 !\n");
//            return;
//        }
//        System.out.println(month);
//        List<Tuple<Users,LocalDate>> filteredUserList = service.getUserFriendsDate(id,month);
//        for(Tuple<Users,LocalDate> usersLocalDateTuple: filteredUserList)
//        {
//            System.out.println("First name: " + usersLocalDateTuple.getLeft().getFirstName() + "|" + "Second name: " + usersLocalDateTuple.getLeft().getSecondName() + "|" + "Date of friendship: " + usersLocalDateTuple.getRight());
//        }
//
//    }
//    private void sendMessage(Long id){
//        String users = "";
//        while(true){
//            System.out.println("Introduce the first name of the user you want to send a message to: ");
//            String firstName = scanner.nextLine();
//            System.out.println("Introduce the flast name of the user you want to send a message to: ");
//            String lastName = scanner.nextLine();
//            try{
//                Long id1 = service.findUser(firstName,lastName);
//                users += id1.toString() + " ";
//            }catch (RepoException e){
//                System.out.println(e.getMessage());
//            }
//            System.out.println("Do you want to send the message to these users?[Y/n]");
//            String response = scanner.nextLine();
//            if(response.equals("Y"))
//                break;
//        }
//        System.out.println("Type message: ");
//        String m = scanner.nextLine();
//        try {
//
//
//                serviceMSG.sendMessage(id, users, m);
//                System.out.println("Message sent!\n");
//        }catch (RepoException|ValidationException|IllegalArgumentException e){
//                    System.out.println(e.getMessage());
//                }
//
//    }
//    private void deleteMessage(Long id){
//        System.out.println("Introduce the first name of the user you want to delete a sent message from: ");
//        String firstName = scanner.nextLine();
//        System.out.println("Introduce the last name of the user you want to delete a sent message from: ");
//        String lastName = scanner.nextLine();
//        Long id1 = 0L;
//        try{
//             id1 = service.findUser(firstName,lastName);
//        }catch (RepoException e){
//            System.out.println(e.getMessage());
//        }
//        System.out.println("Type the message you want to delete: ");
//        String m = scanner.nextLine();
//        try{
//            serviceMSG.deleteMessage(id,id1,m);
//            System.out.println("Message deleted!\n");
//        }catch (RepoException|ValidationException|IllegalArgumentException e){
//            System.out.println(e.getMessage());
//        }
//    }
//    private void replyMessage(Long id){
//        System.out.println("Which message do you want to reply to? ");
//        Long reply = 0L;
//        try{
//            reply  = Long.parseLong(scanner.nextLine());
//        }catch (NumberFormatException ex){
//            System.out.println("Id must be a number!\n");
//            return;
//        }
//
//        try{
//            Message message = serviceMSG.getMessageById(reply);
//            System.out.println("Type message: ");
//            String m = scanner.nextLine();
//            serviceMSG.replyMessage(id,message.getFrom(),m,reply);
//            System.out.println("Reply sent!\n");
//        }catch (RepoException|ValidationException|IllegalArgumentException e){
//            System.out.println(e.getMessage());
//        }
//    }
//    private void updateMessage(Long id){
//        System.out.println("Introduce the first name of the user you want to send an updated message to: ");
//        String firstName = scanner.nextLine();
//        System.out.println("Introduce the last name of the user you want to send an updated message to: ");
//        String lastName = scanner.nextLine();
//        Long id1 = 0L;
//        try{
//            id1 = service.findUser(firstName,lastName);
//        }catch (RepoException e){
//            System.out.println(e.getMessage());
//        }
//        System.out.println("Type the new message you want to send: ");
//        String m = scanner.nextLine();
//        try{
//            serviceMSG.updateMessage(id,id1,m);
//        }catch (RepoException e){
//            System.out.println(e.getMessage());
//        }
//    }
//    private void seeAllMessagesUser(Long id){
//        List<Message> messages = serviceMSG.getMessagesUser(id);
//        for(Message m: messages){
//            System.out.println("From: " + serviceMSG.findUserById(m.getFrom()).getFirstName() + "|" + "Message: " + m.getMessage() + "|" + "Date: " + m.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM--dd HH:mm")));
//            System.out.println("_________________________________________________________");
//        }
//    }
//    private void seeConversation(Long id){
//        System.out.println("Introduce the first name of the user you want to see the conversation with: ");
//        String firstName = scanner.nextLine();
//        System.out.println("Introduce the last name of the user you want to see the conversation with: ");
//        String lastName = scanner.nextLine();
//        Long id1 = 0L;
//        try{
//            id1 = service.findUser(firstName,lastName);
//        }catch (RepoException e){
//            System.out.println(e.getMessage());
//        }
//        List<Message> conversation = serviceMSG.getConversation(id,id1);
//        for(Message m: conversation){
//            System.out.println("From: " + serviceMSG.findUserById(m.getFrom()).getFirstName() + "|" + "Message: " + m.getMessage() + "|" + "Date: " + m.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM--dd HH:mm")));
//            System.out.println("_________________________________________________________");
//        }
//    }
//
//    private void showFriendRequests(){
//        serviceFriendshipRequest.getFriendRequestsTo(idConnectedUser)
//                .stream()
//                .forEach(x->System.out.println(x.toString()));
//    }
//
//    private void rejectFriendRequest(){
//        try{
//            System.out.println("id: ");
//            Long id = Long.parseLong(scanner.nextLine());
//            serviceFriendshipRequest.rejectRequest(id);
//        }catch(Exception e){
//            System.err.println(e.getMessage());
//        }
//    }
//
//    private void approveFriendRequest(){
//        try{
//            System.out.println("id: ");
//            Long id = Long.parseLong(scanner.nextLine());
//            List<FriendshipRequest> fr = serviceFriendshipRequest.getFriendRequestsTo(idConnectedUser)
//                    .stream()
//                    //.forEach(x->System.out.println(x.toString()));
//                    .filter(x->x.getId().equals(id))
//                    .collect(Collectors.toList());
//            Long otherId = fr.get(0).getPair().getFirst();
//            serviceFriendshipRequest.approveRequest(id);
//            service.addFriendship(idConnectedUser, otherId);
//        }catch(Exception e){
//            System.err.println(e.getMessage());
//        }
//    }
//
//    private void login(){
//        System.out.println("Introduce first name: ");
//        String firstName = scanner.nextLine();
//        System.out.println("Introduce last name: ");
//        String lastName = scanner.nextLine();
//        try{
//            idConnectedUser = service.findUser(firstName,lastName);
//        }catch (RepoException e){
//            System.out.println(e.getMessage());
//            System.out.println("Login failed!\n");
//            return;
//        }
//        System.out.println("Welcome, " + firstName + "!");
//        String cmd = "";
//        menuUser();
//        while(true){
//            cmd = scanner.nextLine();
//            switch (cmd){
//                case "exit":
//                {
//                    System.out.println("Logging out...");
//                    idConnectedUser = 0L;
//                    return;
//                }
//                case "reject friend request":{
//                    rejectFriendRequest();
//                    break;
//                }
//                case "approve friend request":{
//                    approveFriendRequest();
//                    break;
//                }
//                case "show friend requests":{
//                    showFriendRequests();
//                    break;
//                }
//                case "add friend":
//                {
//                    addFriendshipUI();
//                    break;
//                }
//                case "remove friend":
//                {
//                    removeFriendshipUI();
//                    break;
//                }
//                case "show user friends":
//                {
//                    getAllFriendsUser(idConnectedUser);
//                    break;
//                }
//                case "show user friends by month":
//                {
//                    filterUserFriendsAfterMonth(idConnectedUser);
//                    break;
//                }
//                case "send message":{
//                    sendMessage(idConnectedUser);
//                    break;
//                }
//                case "reply":
//                {
//                    replyMessage(idConnectedUser);
//                    break;
//                }
//                case "see conversation":
//                {
//                    seeConversation(idConnectedUser);
//                    break;
//                }
//                case "see all messages":
//                {
//                    seeAllMessagesUser(idConnectedUser);
//                    break;
//                }
//                case "delete message":
//                {
//                    deleteMessage(idConnectedUser);
//                    break;
//                }
//                case "update message":
//                {
//                    updateMessage(idConnectedUser);
//                    break;
//                }
//                default:
//                {
//                    System.out.println("Command does not exist!\n");
//                    break;
//                }
//            }
//        }
//
//    }
//    public void start() {
//        String cmd = "";
//        menu();
//        while (true) {
//            System.out.println(">>>");
//            cmd = scanner.nextLine();
//            switch (cmd){
//                case "1":{
//                    showFriendsByUserId();
//                    break;
//                }
//                case "menu":{
//                    menu();
//                    break;
//                }
//                case "add user" :
//                {
//                    addUserUI();
//                    break;
//                }
//                case "login" :
//                {
//                    login();
//                    break;
//                }
//                case "remove user" :
//                {
//                    removeUserUI();
//                    break;
//                }
//                case "update user":{
//                    updateUserUI();
//                    break;
//                }
//                case "show users":
//                {
//                    getAllUsers();
//                    break;
//                }
//                case "show friends":
//                {
//                    getAllFriendships();
//                    break;
//                }
//                case "most social":
//                {
//                    mostSocialCommunity();
//                    break;
//                }
//                case "exit":
//                    {
//                        System.out.println("Exiting application...");
//                        return;
//                    }
//                case "show communities":
//                {
//                    nrOfCommunities();
//                    getAllCommunities();
//                    break;
//                }
//                default:
//                {
//                    System.out.println("Command does not exist!\n");
//                    break;
//                }
//            }
//
//        }
//    }
}
