package com.example.map225theodorrobertgui.pagination;

import java.util.List;


public class Pagination<T> {

    private int pageSize;
    private int paginationSize;
    private int currentPage ;
    private List<T> list = null;

    public Pagination(List<T> list, int pageSize, int paginationSize) {
        this.list = list;
        this.currentPage = 0;
        this.pageSize = pageSize;
        this.paginationSize = paginationSize;
    }

    public int getPageSize(){
        return pageSize;
    }

    public int getPaginationSize(){
        return paginationSize;
    }

    public int getSize() {
        return list == null ? 0 : list.size();
    }

    public int getOffset() {
        return getPageSize() * getCurrentPage();
    }

    public int getLimit() {
        return getPageSize();
    }

    public List<T> getItemsOnCurrentPage() {
        return list.subList(getOffset(), getOffset()+getSizeOfCurrentPage());
    }

    public int getSizeOfCurrentPage(){
        if(getSize() - getOffset() > getPageSize()){
            return getPageSize();
        }
        return getSize() - getOffset();
    }

    public int getPageCount() {
        return getSize() / getPageSize() + (getSize() % getPageSize() > 0 ? 1 : 0);
    }

    public int getActualPaginationSize() {
        return Math.min(getPaginationSize(), getPageCount());
    }

    public int getLastPage() {
        return getPageCount() - 1;
    }

    public int getStartPage() {
        if (getPageCount() > getPaginationSize()
                && getCurrentPage() > getPaginationSize() / 2) {
            return Math.min(getCurrentPage() - getPaginationSize() / 2,
                    getPageCount() - getActualPaginationSize());
        } else {
            return 0;
        }
    }

    public int getEndPage() {
        return Math.min(getStartPage() + getPaginationSize(), getPageCount()) - 1;
    }

    public int getCurrentPage(){
        return currentPage;
    }

    public void setCurrentPage(int newPage) {
        if (newPage < 0) newPage = 0;
        if (newPage > getLastPage()) newPage = getLastPage();
        this.currentPage = newPage;
    }

    public boolean isHasPreviousPage() {
        return hasPreviousPage();
    }

    public boolean hasPreviousPage() {
        return getCurrentPage() > 0;
    }

    public boolean isHasNextPage() {
        return hasNextPage();
    }

    public boolean hasNextPage() {
        return getCurrentPage() < getPageCount() - 1;//getEndPage() < getLastPage();
    }

    public boolean isShouldShowFirst() {
        return shouldShowFirst();
    }

    public boolean shouldShowFirst() {
        return getStartPage() - getPaginationSize() / 2 > 0;
    }

    public boolean isShouldShowLast() {
        return shouldShowLast();
    }

    public boolean shouldShowLast() {
        return getEndPage() + getPaginationSize() / 2 < getLastPage();
    }

}
