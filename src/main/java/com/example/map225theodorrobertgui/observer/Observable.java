package com.example.map225theodorrobertgui.observer;

import javafx.beans.InvalidationListener;

import java.util.ArrayList;
import java.util.List;

public class Observable{
    private List<Observer> observers = new ArrayList<>();

    protected void notifyObservers(){
        for(Observer o: observers){
            o.update();
        }
    }
    public void addObserver(Observer o){
        observers.add(o);
    }
    public void removeObserver(Observer o){
        observers.remove(o);
    }
}
