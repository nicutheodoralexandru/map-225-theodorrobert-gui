package com.example.map225theodorrobertgui.observer;

import java.util.Observable;

public abstract class Observer  {
    public abstract void update();
}
