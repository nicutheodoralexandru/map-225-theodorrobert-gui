package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.*;
import com.example.map225theodorrobertgui.domain.validators.*;
import com.example.map225theodorrobertgui.observer.Observer;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.EventDatabase;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceEvent;
import com.example.map225theodorrobertgui.service.ServiceReports;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Events extends Observer {
    private MainApp mainApp;
    private Stage thisStage;
    private Service service;
    private ServiceReports serviceReports;
    private ServiceEvent serviceEvent;
    private long selectedEventId;
    ObservableList<EventUI> events = FXCollections.observableArrayList();

    @FXML
    private TableColumn<EventUI,LocalDate> tableColumnDate;
    @FXML
    private TableColumn<EventUI,String> tableColumnLocation;
    @FXML
    private TableColumn<EventUI,String> tableColumnAtendeesName;
    @FXML
    private TableColumn<EventUI,String> tableColumnOrganizer;
    @FXML
    private TableView<EventUI> tableViewMyEvents;
    @FXML
    private Button addAtendeeButton;
    @FXML
    private Button newEventButton;
    @FXML
    private Button goBackButton;
    @FXML
    private Button cancelEventButton;
    @FXML
    private TextField textFieldLastName;
    @FXML
    private TextField textFieldFirstName;
    @FXML
    private TextField textFieldLocation;
    @FXML
    private DatePicker datePicker;

    @FXML
    private void initialize(){
        onClickAddAtendeeButton();
        onClickGoBackButton();
        onClickNewEventButton();
        onClickCancelEventButton();

        serviceEvent.addObserver(this);
        try {
          tableColumnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
            tableColumnLocation.setCellValueFactory(new PropertyValueFactory<>("location"));
            tableColumnAtendeesName.setCellValueFactory(new PropertyValueFactory<>("atendeesName"));
            tableColumnOrganizer.setCellValueFactory(new PropertyValueFactory<>("organizerName"));
          refreshEventList();
          tableViewMyEvents.setItems(events);
            tableViewMyEvents.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<EventUI>() {
               @Override
                public void changed(ObservableValue<? extends EventUI> observable, EventUI oldValue, EventUI newValue) {
                   if(newValue != null) {
                       selectedEventId = newValue.getId();
                   }
                   else{
                       selectedEventId = -1;
                   }
               }
            });
        }catch (RepoException e){
            e.printStackTrace();
        }
    }

    private void onClickCancelEventButton(){
        EventHandler<ActionEvent> cancelEvent = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    serviceEvent.cancelEvent(selectedEventId, mainApp.getUserID());
                    refreshEventList();
                } catch (Exception e) {
                    showErrorPopUp("You must select an event to cancel!");
                }
            }
        };
        cancelEventButton.setOnAction(cancelEvent);
    }

    private void onClickAddAtendeeButton(){
        EventHandler<ActionEvent> addAtendee = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    String firstName = textFieldFirstName.getText();
                    String lastName = textFieldLastName.getText();
                    long u = service.findUserByFirstName(firstName, lastName);
                    serviceEvent.addAtendee(selectedEventId, u);
                    refreshEventList();
                } catch (Exception e) {
                    showErrorPopUp(e.getMessage());
                }
            }
        };
        addAtendeeButton.setOnAction(addAtendee);
    }

    private void onClickNewEventButton(){
        EventHandler<ActionEvent> newEvent = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    String location = textFieldLocation.getText();
                    LocalDate date = datePicker.getValue();
                    serviceEvent.createNewEvent(mainApp.getUserID(), date, location);
                    refreshEventList();
                } catch (Exception e) {
                    showErrorPopUp(e.getMessage());
                }
            }
        };
        newEventButton.setOnAction(newEvent);
    }

    private void onClickGoBackButton(){
        Stage stage = thisStage;
        EventHandler<ActionEvent> back = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main-app.fxml"));
                    fxmlLoader.setController(mainApp);
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("MainMenu");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        };
        goBackButton.setOnAction(back);
    }

    public Events(MainApp mainApp,Stage stage){
        thisStage = stage;
        this.mainApp = mainApp;
        setService();
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("events.fxml"));
            fxmlLoader.setController(this);
            Parent root = (Parent) fxmlLoader.load();

            thisStage.setTitle("Events");
            thisStage.setScene(new Scene(root));
            thisStage.show();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void ShowStage(){
        thisStage.show();
        //update();
    }

    public void setService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Repository<Long, Message> repoMessage = new MessageDatabase(url, username, password, new MessageValidator());
        EventDatabase repoEvents = new EventDatabase(url, username, password, new EventValidator());
        service = new Service(repoUser,repoFriend);
        serviceReports = new ServiceReports(repoMessage, repoFriend, repoUser);
        serviceEvent = new ServiceEvent(repoEvents, service);
    }

    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();
    }

    private void refreshEventList(){
        //List<Event> events = serviceEvent.getUserEvents(mainApp.getUserID());
        List<EventUI> events = serviceEvent.getUserEventsUI(mainApp.getUserID());
        this.events.setAll(events);
    }

    @Override
    public void update() {
        refreshEventList();
    }
}
