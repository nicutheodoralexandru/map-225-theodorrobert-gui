package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.main.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Database.initMysql();
        try {
            Font.loadFont(
                    HelloApplication.class.getResource("fonts/Lexend-ExtraBold.ttf").toExternalForm(),
                    17
            );
            Font.loadFont(
                    HelloApplication.class.getResource("fonts/Lexend-Bold.ttf").toExternalForm(),
                    17
            );
            Font.loadFont(
                    HelloApplication.class.getResource("fonts/Lexend-Regular.ttf").toExternalForm(),
                    17
            );
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("log-in.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Log in");
            stage.setScene(scene);
            stage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch();
    }
}