package com.example.map225theodorrobertgui.utils;

import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.repository.Repository;

import java.util.*;

/**
 * Class for determining the number of communities
 */
public class Communities {

    private Iterable<Users> users;
    private Iterable<Friendship> friendships;
    private boolean[][] adjMatrix;
    private long[] visited;
    private Map<Long,Long> map;
    private Map<Long,Long> unmap;
    private int nrUsers;

    /**
     * Constructor for class Communities
     * @param repoUser: user repository used for getting all the users
     * @param repoFriend: friendship repository for getting all the friendships
     */
    public Communities(Repository<Long,Users> repoUser, Repository<Long,Friendship> repoFriend) {
        this.users = repoUser.findAll();
        this.friendships = repoFriend.findAll();
        map = new HashMap<>();
        unmap = new HashMap<>();
        this.nrUsers = repoUser.size();
        this.visited = new long[(int)nrUsers];
        this.adjMatrix = new boolean[(int)nrUsers][];
        for(int i=0;i<nrUsers;i++){
            this.adjMatrix[i] = new boolean[(int)nrUsers];
            Arrays.fill(adjMatrix[i],false);
        }
        edges();

    }

    private void DFS(int node, int currentCommunity){
        visited[node] = currentCommunity;
        for(int i=0; i<nrUsers;i++)
            if(adjMatrix[node][i] && visited[i]==0){
                DFS(i,currentCommunity);
            }
    }

    private void edges(){

        long i = 0L;
        for(Users user: users){
            map.put(user.getId(),i);
            unmap.put(i, user.getId());
            i++;
        }
        for(Friendship friendship: friendships){
            long left = map.get(friendship.getPair().getFirst());
            long right = map.get(friendship.getPair().getSecond());
            adjMatrix[(int)left][(int)right] = true;
            adjMatrix[(int)right][(int)left] = true;
        }
    }

    /**
     * Function that returns the number of communities
     * @return returns the number of communities
     */
    public int nrOfCommunities(){
        int nrCommunities = 0;
        Arrays.fill(visited,0);
        for(int i=0; i< nrUsers; i++)
            if(visited[i] == 0){
                nrCommunities++;
                DFS(i,nrCommunities);
            }
        return nrCommunities;
    }

    /**
     * Function that returns the users for each community
     * @param repoUser: repository of users for finding the users in each community
     * @return returns a map with all the communities
     */
    public Map<Integer,List<Users>> getAllCommunities(Repository<Long, Users> repoUser) {
        Map<Integer, List<Users>> usersMap = new HashMap<>();
        for (int i = 1; i <= nrOfCommunities(); i++) {
            List<Users> userList = new ArrayList<>();
            for (int j = 0; j < nrUsers; j++)
                if (visited[j] == i) {
                    userList.add(repoUser.findOne(unmap.get((long)j)));
                }
            usersMap.put(i,userList);
        }
        return usersMap;
    }

    /**
     * Function that returns the most social community
     * @return returns the most social community
     */
    public int mostSocialCommunity(){
        int max = 0;
        int mostSocial=0;
        for(int i = 1;i<=nrOfCommunities();i++){
            int counter = 0;
            for(int j=0;j<nrUsers;j++)
                if(visited[j] == i){
                    counter++;
                }
            if(counter>max){
                max = counter;
                mostSocial = i;
            }
        }
        return mostSocial;
    }

}
