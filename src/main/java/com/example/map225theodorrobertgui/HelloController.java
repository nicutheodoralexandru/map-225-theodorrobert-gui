package com.example.map225theodorrobertgui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HelloController {


    @FXML
    private VBox layoutVBox;
    @FXML
    private ImageView imageViewSocialNetwork;
    @FXML
    private Button signInButton;
    @FXML
    private Button logInButton;
    @FXML
    public void initialize(){
        layoutVBox.widthProperty().addListener(h->
                imageViewSocialNetwork.setFitWidth(layoutVBox.getWidth()));
    }
    public void onSignInButtonClick(ActionEvent actionEvent) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sign-in.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("MainMenu");
            stage.setScene(new Scene(root));
            stage.show();
            Stage stage1 = (Stage) signInButton.getScene().getWindow();
            stage1.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onLogInButtonClick(ActionEvent actionEvent) {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("log-in.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Login");
            stage.setScene(new Scene(root));
            stage.show();
            Stage stage1 = (Stage) logInButton.getScene().getWindow();
            stage1.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}