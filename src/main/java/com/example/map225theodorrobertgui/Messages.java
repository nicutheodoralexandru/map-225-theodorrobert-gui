package com.example.map225theodorrobertgui;

import com.example.map225theodorrobertgui.config.Database;
import com.example.map225theodorrobertgui.domain.Friendship;
import com.example.map225theodorrobertgui.domain.Message;
import com.example.map225theodorrobertgui.domain.Tuple;
import com.example.map225theodorrobertgui.domain.Users;
import com.example.map225theodorrobertgui.domain.validators.*;
import com.example.map225theodorrobertgui.pagination.Pagination;
import com.example.map225theodorrobertgui.repository.Repository;
import com.example.map225theodorrobertgui.repository.database.FriendshipDatabase;
import com.example.map225theodorrobertgui.repository.database.MessageDatabase;
import com.example.map225theodorrobertgui.repository.database.UserDatabase;
import com.example.map225theodorrobertgui.service.Service;
import com.example.map225theodorrobertgui.service.ServiceFriendshipRequest;
import com.example.map225theodorrobertgui.service.ServiceMessage;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Messages {
    private MainApp mainApp;
    private Stage thisStage;
    private Service service;
    private ServiceMessage serviceMessages;
    private Pagination<Message> usersPagination;
    private int pageSize = 5;
    private int paginationSize = 5;
    ObservableList<Message> messages = FXCollections.observableArrayList();

    @FXML
    private Button replyAllButton;
    @FXML
    private Button replyButton;
    @FXML
    private Button sendMessageButton;
    @FXML
    private TableView tableMessages;
    @FXML
    private TableColumn<Message,String> columnFrom;
    @FXML
    private TableColumn<Message,String> columnTo;
    @FXML
    private TableColumn columnMessage;
    @FXML
    private TableColumn columnDate;
    @FXML
    private TextField textFieldFrom;
    @FXML
    private TextField textFieldTo;
    @FXML
    private TextArea textAreaMessage;
    @FXML
    private Label labelReply;
    @FXML
    private TextArea textAreaReply;
    @FXML
    private Button sendReplyButton;
    @FXML
    private TableColumn columnID;
    @FXML
    private Label labelID;
    @FXML
    private Button backButton;
    @FXML
    private Button replyAllButtonUser;
    @FXML
    private Button prevButton;
    @FXML
    private Button nextButton;
    @FXML
    private Label labelPageNumber;

    @FXML
    private void initialize() {
        Long id = mainApp.getUserID();
        labelReply.setVisible(false);
        textAreaReply.setVisible(false);
        sendReplyButton.setVisible(false);
        replyAllButtonUser.setVisible(false);
        labelID.setVisible(false);
        textAreaMessage.setEditable(false);
        textFieldFrom.setEditable(false);
        textFieldTo.setEditable(false);
        onReplyButtonClick();
        onReplyAllButtonClick();
        onSendMessageButtonClick();
        onNextButtonClick();
        onPrevButtonClick();
        goBackButton();
        try{
            List<Message> userMessages = serviceMessages.getMessagesUser(id.toString());

            columnID.setCellValueFactory(new PropertyValueFactory<Message,Long>("id"));
            columnID.setVisible(false);
            columnFrom.setCellValueFactory(c->new SimpleStringProperty(service.findOne(c.getValue().getFrom()).getUserName()));
            columnTo.setCellValueFactory(c->new SimpleStringProperty(service.getUsernames(c.getValue().getTo())));
            columnMessage.setCellValueFactory(new PropertyValueFactory<Message,String>("message"));
            columnDate.setCellValueFactory(new PropertyValueFactory<Message, LocalDateTime>("date"));
            refreshFriendList();
            tableMessages.setItems(messages);

            tableMessages.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Message>() {

                @Override
                public void changed(ObservableValue<? extends Message> observable, Message oldValue, Message newValue) {
                    if(newValue!=null){
                        textFieldFrom.setText(service.findOne(newValue.getFrom()).getUserName());
                        textFieldTo.setText(service.getUsernames(newValue.getTo()));
                        textAreaMessage.setText(newValue.getMessage());
                        labelID.setText(newValue.getId().toString());
                    }
                    else{
                        textFieldFrom.setText("");
                        textFieldTo.setText("");
                        textAreaMessage.setText("");
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public Messages(MainApp mainApp,Stage stage) {
        this.mainApp = mainApp;
        thisStage = stage;
        setService();
        setMessagesService();
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("text.fxml"));
            fxmlLoader.setController(this);
            Parent root = (Parent) fxmlLoader.load();

            thisStage.setTitle("Messages");
            thisStage.setScene(new Scene(root));
            thisStage.show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void ShowStage() {
        thisStage.show();
    }
    public void loadTablePage(){

        messages.setAll(usersPagination.getItemsOnCurrentPage());
        nextButton.setDisable(!this.usersPagination.hasNextPage());
        prevButton.setDisable(!this.usersPagination.hasPreviousPage());
    }
    private void refreshFriendList(){
        List<Message> userMessages = serviceMessages.getMessagesUser(mainApp.getUserID().toString());
        usersPagination = new Pagination<>(userMessages,pageSize,paginationSize);
        loadTablePage();
    }
    public void onNextButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasNextPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()+1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        nextButton.setOnAction(eventHandler);
    }
    public void onPrevButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(usersPagination.hasPreviousPage()){

                    usersPagination.setCurrentPage(usersPagination.getCurrentPage()-1);
                    labelPageNumber.setText((usersPagination.getCurrentPage()+1) + "");
                    loadTablePage();
                }
            }
        };
        prevButton.setOnAction(eventHandler);
    }


    public void setService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        service = new Service(repoUser,repoFriend);
    }

    public void setMessagesService(){
        String url = Database.url;
        String username = Database.username;
        String password = Database.password;
        Repository<Long, Users> repoUser = new UserDatabase(url, username,password, new UserValidator());
        Repository<Long, Friendship> repoFriend = new FriendshipDatabase(url, username, password, new FriendshipValidator());
        Repository<Long, Message> repoMessages = new MessageDatabase(url, username, password, new MessageValidator());
        serviceMessages = new ServiceMessage(repoMessages, repoUser, repoFriend);
    }

    private void onReplyButtonClick() {
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                labelReply.setVisible(true);
                textAreaReply.setVisible(true);
                sendReplyButton.setVisible(true);



            }
        };
        EventHandler<ActionEvent> eventHandler1 = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    Long from = mainApp.getUserID();
                    Long to = -1L;
                    Long reply = -1L;
                    if(textFieldFrom.getText().equals("")||labelID.getText().equals(""))
                    {
                        showErrorPopUp("Please select a message to reply to!");
                    }
                    else
                    {
                        reply = Long.parseLong(labelID.getText());
                        Message message = serviceMessages.getMessageById(reply);
                         to = message.getFrom();
                        System.out.println(to);
                        System.out.println(from);

                    }
                    String message  = textAreaReply.getText();
                    if(reply != -1L && to != -1L) {
                        serviceMessages.replyMessage(from, to, message, reply);

                        labelReply.setVisible(false);
                        textAreaReply.setVisible(false);
                        sendReplyButton.setVisible(false);
                    }
                }catch (RepoException| ValidationException e){
                    showErrorPopUp(e.getMessage());
                }

            }
        };

        replyButton.setOnAction(eventHandler);
        sendReplyButton.setOnAction(eventHandler1);
    }
    private void onReplyAllButtonClick(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                labelReply.setVisible(true);
                textAreaReply.setVisible(true);
                sendReplyButton.setVisible(false);
                replyAllButtonUser.setVisible(true);
            }
        };
        EventHandler<ActionEvent> eventHandler1 = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    Long from = mainApp.getUserID();
                    Long reply = -1L;

                    if(labelID.getText().equals(""))
                    {
                        showErrorPopUp("Please select a message to reply to!");
                    }
                    else
                    {
                        reply = Long.parseLong(labelID.getText());
                    }
                    Message messageUser = serviceMessages.getMessageById(reply);
                    String to = messageUser.getTo();
                    String[] parts = to.split(";");

                    String replyTo = messageUser.getFrom().toString();
                    System.out.println(from);
                    for(String part:parts)
                    {
                        System.out.println(part);
                        if(!part.equals(from.toString()))
                            replyTo += ";"+ part;

                    }
                    System.out.println(replyTo);

                    String message  = textAreaReply.getText();
                    if(reply != -1L ) {


                        serviceMessages.replyAll(from,replyTo,message,reply);
                        labelReply.setVisible(false);
                        textAreaReply.setVisible(false);
                        replyAllButtonUser.setVisible(false);
                    }
                }catch (RepoException| ValidationException e){
                    showErrorPopUp(e.getMessage());
                }

            }
        };


        replyAllButton.setOnAction(eventHandler);
        replyAllButtonUser.setOnAction(eventHandler1);
    }
    public static void showErrorPopUp(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("An error has occurred!");
        alert.setContentText(message);
        alert.show();

    }
    private void onSendMessageButtonClick(){

        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("send-message.fxml"));
                    fxmlLoader.setController( new SendMessage(mainApp,thisStage));
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("Messages");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();
                    //stage.close();
                } catch (Exception e){
                e.printStackTrace();
            }
            }
        };
        sendMessageButton.setOnAction(eventHandler);
    }
    private void goBackButton(){
        EventHandler<ActionEvent> event1 = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main-app.fxml"));
                    fxmlLoader.setController(mainApp);
                    Parent root = (Parent) fxmlLoader.load();

                    thisStage.setTitle("MainMenu");
                    thisStage.setScene(new Scene(root));
                    thisStage.show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        backButton.setOnAction(event1);
    }

}
