module com.example.map225theodorrobertgui {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.pdfbox;
    requires org.apache.fontbox;
    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens com.example.map225theodorrobertgui to javafx.fxml;
    exports com.example.map225theodorrobertgui;
    exports com.example.map225theodorrobertgui.service;
    opens com.example.map225theodorrobertgui.service to javafx.fxml;
    opens com.example.map225theodorrobertgui.domain to javafx.fxml,javafx.base,javafx.graphics;
    exports com.example.map225theodorrobertgui.main;
    opens com.example.map225theodorrobertgui.main to javafx.fxml;
    exports com.example.map225theodorrobertgui.ui;
    opens com.example.map225theodorrobertgui.ui to javafx.fxml;
}